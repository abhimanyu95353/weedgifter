package com.admin.weedgifter.utils;
import android.app.Application;
import com.admin.weedgifter.R;
import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import io.branch.referral.Branch;

@ReportsCrashes(mailTo = "reports@yourdomain.com",
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.app_name)

public class CustomApplicationClass extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //initialising ACRA- Crash Report
        ACRA.init(this);
        //inititalising Branch.io for Deep linking
        Branch.getAutoInstance(this);
    }
}
