package com.admin.weedgifter.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.activities.AccountActivity;
import com.admin.weedgifter.activities.SharePostWithFriendsActivity;
import com.admin.weedgifter.adapters.LikedPeopleListAdapter;

import org.w3c.dom.Text;

import static java.security.AccessController.getContext;

public class Dialog {

    public static void giftToMyselfDialog(Context context) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mView = inflater.inflate(R.layout.success_dialog, null);
        alert.setView(mView);
        final AlertDialog alertDialog = alert.create();
        alertDialog.setCanceledOnTouchOutside(false);
        TextView textInfo = mView.findViewById(R.id.textInfo);
        Button button = mView.findViewById(R.id.btnBack);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }
    public static void giftToFriendDialog(Context context) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mView = inflater.inflate(R.layout.gift_to_friend_dialog, null);
        alert.setView(mView);
        final AlertDialog alertDialog = alert.create();
        alertDialog.setCanceledOnTouchOutside(false);
        Button button = mView.findViewById(R.id.btnBack);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }
    public static void newAccountDialog(final Context context) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mView = inflater.inflate(R.layout.success_dialog, null);
        alert.setView(mView);
        final AlertDialog alertDialog = alert.create();
        alertDialog.setCanceledOnTouchOutside(false);
        TextView textInfo = mView.findViewById(R.id.textInfo);
        textInfo.setText(context.getText(R.string.new_account_added));
        textInfo.setTextColor(Color.parseColor("#22B573"));
        Button button = mView.findViewById(R.id.btnBack);
        button.setText(context.getText(R.string.back_to_list));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity) context).finish();
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }
    public static void passwordChangedDialog(final Context context) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mView = inflater.inflate(R.layout.success_dialog, null);
        alert.setView(mView);
        final AlertDialog alertDialog = alert.create();
        alertDialog.setCanceledOnTouchOutside(false);
        TextView textInfo = mView.findViewById(R.id.textInfo);
        TextView titleText = mView.findViewById(R.id.titleText);
        titleText.setText(context.getText(R.string.password_change));
        textInfo.setText(context.getText(R.string.password_changed_successfully));
        textInfo.setTextColor(Color.parseColor("#22B573"));
        Button button = mView.findViewById(R.id.btnBack);
        button.setText(context.getText(R.string.back));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity) context).finish();
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }
    public static void accountUdpatedDialog(final Context context) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mView = inflater.inflate(R.layout.success_dialog, null);
        alert.setView(mView);
        final AlertDialog alertDialog = alert.create();
        alertDialog.setCanceledOnTouchOutside(false);
        TextView textInfo = mView.findViewById(R.id.textInfo);
        TextView titleText = mView.findViewById(R.id.titleText);
        titleText.setText(context.getText(R.string.congratulation));
        textInfo.setText(context.getText(R.string.account_update_success));
        textInfo.setTextColor(Color.parseColor("#22B573"));
        Button button = mView.findViewById(R.id.btnBack);
        button.setText(context.getText(R.string.back));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity) context).finish();
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }
    public static void sharePostWithFriendsDialog(final Context context) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mView = inflater.inflate(R.layout.dialog_share_post_with_friends, null);
        alert.setView(mView);
        final AlertDialog alertDialog = alert.create();
        LinearLayout lineImg = mView.findViewById(R.id.lineImg);
        TextView tvShare = mView.findViewById(R.id.tvShare);
        tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SharePostWithFriendsActivity.class);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        });
        lineImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#0C0B0B")));
        Window window = alertDialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        window.setAttributes(wlp);
        // This flag is to remove DIM backgorund of dialog
        // wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
    }
    public static void likedPeopleDialog(final Context context) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mView = inflater.inflate(R.layout.dialog_liked_people_list, null);
        alert.setView(mView);
        final AlertDialog alertDialog = alert.create();
        RecyclerView likedView = mView.findViewById(R.id.likedView);
        LinearLayout lineImg = mView.findViewById(R.id.lineImg);
        LikedPeopleListAdapter likedPeopleListAdapter = new LikedPeopleListAdapter(context);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        likedView.setLayoutManager(linearLayoutManager);
        likedView.setAdapter(likedPeopleListAdapter);
        likedPeopleListAdapter.notifyDataSetChanged();
        lineImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#0C0B0B")));
        Window window = alertDialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        window.setAttributes(wlp);
    }
}
