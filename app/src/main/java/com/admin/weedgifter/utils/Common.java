package com.admin.weedgifter.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;

import com.admin.weedgifter.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("ALL")
public class Common {
    public static void displayToast(Context context, String toastMsg) {
        Toast.makeText(context, toastMsg, Toast.LENGTH_LONG).show();
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static AlertDialog showLoadingProgress(Activity context) {
        final AlertDialog alertDialog;
        final AlertDialog.Builder builder;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.progress_loader, null, false);
        AVLoadingIndicatorView avLoadingIndicatorView = (AVLoadingIndicatorView) layout.findViewById(R.id.progress_avi);
        avLoadingIndicatorView.show();
        builder = new AlertDialog.Builder(context, R.style.progressDialog);
        builder.setView(layout);
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return alertDialog;
    }
    public static boolean isEmailValid(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches())
            return true;
        else
            return false;
    }
    public static boolean isPasswordValid(String password) {
        if (password.length() < 8) {
            return false;
        } else {
            return true;
        }
    }
    public static boolean isConfirmPassword(String confrimPassword) {
        if (confrimPassword.length() < 8) {
            return false;
        } else {
            return true;
        }
    }
    public static boolean isCardNumberValid(String cardNumber) {
        if (cardNumber.length() < 16) {
            return false;
        } else {
            return true;
        }
    }
    public static boolean isCvvValid(String cvv) {
        if (cvv.length() < 3) {
            return false;
        } else {
            return true;
        }
    }
    public static void toastMsg(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }
    public static void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
//    public static boolean isCurrentPassword(String currentPassword) {
//        if (cvv.length() < 3) {
//            return false;
//        } else {
//            return true;
//        }
//    }
}
