package com.admin.weedgifter.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.activities.StoresActivity;
import com.admin.weedgifter.adapters.FeaturedItemsAdapter;
import com.admin.weedgifter.utils.Common;

public class HomeFragment extends Fragment {
    private TextView featuredViewAll, deliveriesViewAll, merchandiseViewAll;
    private ImageView storefrontMap, doctorsMap;
    private RecyclerView featuredView, deliveriesView, merchandiseView, storeFrontView, doctorsView;
    private Button btnDiscover;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        //    AppCompatActivity activity = (AppCompatActivity) getActivity();
        //   activity.setSupportActionBar(toolbar);
        //toolbar.setTitle("SASdfsf");
//        toolbar.setLogo(R.drawable.logo);
        toolbar.setLogo(ContextCompat.getDrawable(getActivity(),R.drawable.action_bar_logo));
        initViews(view);
        clickListner();
        setFeaturedView();
        setDeliveriesView();
        setMerchandiseView();
        setStoreFrontView();
        setDoctorsView();
        Common.hideKeyboard(getContext(), view);
        return view;
    }

    private void clickListner() {
        btnDiscover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), StoresActivity.class);
                startActivity(intent);
            }
        });
        featuredViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), StoresActivity.class);
                startActivity(intent);
            }
        });
        deliveriesViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), StoresActivity.class);
                startActivity(intent);
            }
        });
        merchandiseViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), StoresActivity.class);
                startActivity(intent);
            }
        });
        storefrontMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), StoresActivity.class);
                startActivity(intent);
            }
        });
        doctorsMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), StoresActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setDoctorsView() {
        String layoutType = "doctorsView";
        FeaturedItemsAdapter featuredItemsAdapter = new FeaturedItemsAdapter(getContext(), layoutType);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        storeFrontView.setLayoutManager(linearLayoutManager);
        storeFrontView.setAdapter(featuredItemsAdapter);
        featuredItemsAdapter.notifyDataSetChanged();
    }

    private void setStoreFrontView() {
        String layoutType = "storefrontView";
        FeaturedItemsAdapter featuredItemsAdapter = new FeaturedItemsAdapter(getContext(), layoutType);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        doctorsView.setLayoutManager(linearLayoutManager);
        doctorsView.setAdapter(featuredItemsAdapter);
        featuredItemsAdapter.notifyDataSetChanged();
    }

    private void setMerchandiseView() {
        String layoutType = "merchandiseView";
        FeaturedItemsAdapter featuredItemsAdapter = new FeaturedItemsAdapter(getContext(), layoutType);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        merchandiseView.setLayoutManager(linearLayoutManager);
        merchandiseView.setAdapter(featuredItemsAdapter);
        featuredItemsAdapter.notifyDataSetChanged();
    }

    private void setDeliveriesView() {
        String layoutType = "deliveriesView";
        FeaturedItemsAdapter featuredItemsAdapter = new FeaturedItemsAdapter(getContext(), layoutType);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        deliveriesView.setLayoutManager(linearLayoutManager);
        deliveriesView.setAdapter(featuredItemsAdapter);
        featuredItemsAdapter.notifyDataSetChanged();
    }

    private void setFeaturedView() {
        String layoutType = "featuredView";
        FeaturedItemsAdapter featuredItemsAdapter = new FeaturedItemsAdapter(getContext(), layoutType);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        featuredView.setLayoutManager(linearLayoutManager);
        featuredView.setAdapter(featuredItemsAdapter);
        featuredItemsAdapter.notifyDataSetChanged();
    }

    private void initViews(View view) {
        featuredView = view.findViewById(R.id.featuredView);
        deliveriesView = view.findViewById(R.id.deliveriesView);
        merchandiseView = view.findViewById(R.id.merchandiseView);
        storeFrontView = view.findViewById(R.id.storeFrontView);
        doctorsView = view.findViewById(R.id.doctorsView);
        btnDiscover = view.findViewById(R.id.btnDiscover);
        featuredViewAll = view.findViewById(R.id.featuredViewAll);
        merchandiseViewAll = view.findViewById(R.id.merchandiseViewAll);
        deliveriesViewAll = view.findViewById(R.id.deliveriesViewAll);
        storefrontMap = view.findViewById(R.id.storefrontMap);
        doctorsMap = view.findViewById(R.id.doctorsMap);

    }
}