package com.admin.weedgifter.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.admin.weedgifter.R;

public class SocialFragment extends Fragment {
    private TextView posts, requests, timeline;
    private View postsSelected, postsUnselected, requestsSelected, requestsUnselected, timelineSelected, timelineUnselected;
    private FrameLayout postsFrame, requestsFrame, timelineFrame;
    private LinearLayout postsContainer, requestsContainer, timelineContainer;
    private PostsFragment postsFragment = new PostsFragment();
    private TimelineFragment timelineFragment = new TimelineFragment();
    private RequestsFragment requestsFragment = new RequestsFragment();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_social, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setLogo(null);
        initViews(root);
        clickListners();
        return root;
    }
    private void clickListners() {
        postsContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postsFrame.setVisibility(View.VISIBLE);
                timelineFrame.setVisibility(View.GONE);
                requestsFrame.setVisibility(View.GONE);
                postsSelected.setVisibility(View.VISIBLE);
                postsUnselected.setVisibility(View.GONE);
                requestsSelected.setVisibility(View.GONE);
                requestsUnselected.setVisibility(View.VISIBLE);
                timelineSelected.setVisibility(View.GONE);
                timelineUnselected.setVisibility(View.VISIBLE);
                posts.setTextColor(Color.parseColor("#0D9B6B"));
                requests.setTextColor(Color.parseColor("#A7A7A7"));
                timeline.setTextColor(Color.parseColor("#A7A7A7"));
                FragmentTransaction fragementTransaction = getChildFragmentManager().beginTransaction();
                fragementTransaction.replace(R.id.postsFrame,postsFragment);
                fragementTransaction.commit();
            }
        });
        requestsContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postsFrame.setVisibility(View.GONE);
                timelineFrame.setVisibility(View.GONE);
                requestsFrame.setVisibility(View.VISIBLE);
                postsSelected.setVisibility(View.GONE);
                postsUnselected.setVisibility(View.VISIBLE);
                requestsSelected.setVisibility(View.VISIBLE);
                requestsUnselected.setVisibility(View.GONE);
                timelineSelected.setVisibility(View.GONE);
                timelineUnselected.setVisibility(View.VISIBLE);
                posts.setTextColor(Color.parseColor("#A7A7A7"));
                requests.setTextColor(Color.parseColor("#0D9B6B"));
                timeline.setTextColor(Color.parseColor("#A7A7A7"));
                FragmentTransaction fragementTransaction = getChildFragmentManager().beginTransaction();
                fragementTransaction.replace(R.id.requestsFrame,requestsFragment);
                fragementTransaction.commit();
            }
        });
        timelineContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postsFrame.setVisibility(View.GONE);
                timelineFrame.setVisibility(View.VISIBLE);
                requestsFrame.setVisibility(View.GONE);
                postsSelected.setVisibility(View.GONE);
                postsUnselected.setVisibility(View.VISIBLE);
                requestsSelected.setVisibility(View.GONE);
                requestsUnselected.setVisibility(View.VISIBLE);
                timelineSelected.setVisibility(View.VISIBLE);
                timelineUnselected.setVisibility(View.GONE);
                posts.setTextColor(Color.parseColor("#A7A7A7"));
                requests.setTextColor(Color.parseColor("#A7A7A7"));
                timeline.setTextColor(Color.parseColor("#0D9B6B"));
                FragmentTransaction fragementTransaction = getChildFragmentManager().beginTransaction();
                fragementTransaction.replace(R.id.timelineFrame,timelineFragment);
                fragementTransaction.commit();
            }
        });
    }
    private void initViews(View view) {
        posts = view.findViewById(R.id.posts);
        requests = view.findViewById(R.id.requests);
        timeline = view.findViewById(R.id.timeline);
        postsSelected = view.findViewById(R.id.postsSelected);
        postsUnselected = view.findViewById(R.id.postsUnselected);
        requestsSelected = view.findViewById(R.id.requestsSelected);
        requestsUnselected = view.findViewById(R.id.requestsUnselected);
        timelineSelected = view.findViewById(R.id.timelineSelected);
        timelineUnselected = view.findViewById(R.id.timelineUnselected);
        postsFrame = view.findViewById(R.id.postsFrame);
        requestsFrame = view.findViewById(R.id.requestsFrame);
        timelineFrame = view.findViewById(R.id.timelineFrame);
        postsContainer = view.findViewById(R.id.postsContainer);
        requestsContainer = view.findViewById(R.id.requestsContainer);
        timelineContainer = view.findViewById(R.id.timelineContainer);
        postsFrame.setVisibility(View.VISIBLE);
        timelineFrame.setVisibility(View.GONE);
        requestsFrame.setVisibility(View.GONE);
        postsSelected.setVisibility(View.VISIBLE);
        postsUnselected.setVisibility(View.GONE);
        requestsSelected.setVisibility(View.GONE);
        requestsUnselected.setVisibility(View.VISIBLE);
        timelineSelected.setVisibility(View.GONE);
        timelineUnselected.setVisibility(View.VISIBLE);
        posts.setTextColor(Color.parseColor("#0D9B6B"));
        requests.setTextColor(Color.parseColor("#A7A7A7"));
        timeline.setTextColor(Color.parseColor("#A7A7A7"));
        FragmentTransaction fragementTransaction = getChildFragmentManager().beginTransaction();
        fragementTransaction.replace(R.id.postsFrame,postsFragment);
        fragementTransaction.commit();
    }
}