package com.admin.weedgifter.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.FeaturedItemsAdapter;
import com.admin.weedgifter.adapters.InboxListAdapter;

public class MessageFragment extends Fragment {

    private EditText searchBar;
    private RecyclerView inboxView;
    private ImageView close;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_message, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setLogo(null);

        searchBar = root.findViewById(R.id.searchBar);
        inboxView  = root.findViewById(R.id.inboxView);
        setInobxView();
        searchBar.setHint(getText(R.string.search_friend));
        return root;
    }
    private void setInobxView() {
        InboxListAdapter featuredItemsAdapter = new InboxListAdapter(getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        inboxView.setLayoutManager(linearLayoutManager);
        inboxView.setAdapter(featuredItemsAdapter);
        featuredItemsAdapter.notifyDataSetChanged();
    }

}
