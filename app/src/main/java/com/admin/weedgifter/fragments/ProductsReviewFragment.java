package com.admin.weedgifter.fragments;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.StoresAndProductsReviewAdapter;

public class ProductsReviewFragment extends Fragment {
    private RecyclerView productsReview;
    private StoresAndProductsReviewAdapter storesAndProductsReviewAdapter;
    String type = "products";

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_products_review, container, false);
        initViews(root);
        setStoreReviewList();
        return root;
    }
    private void setStoreReviewList() {
        storesAndProductsReviewAdapter = new StoresAndProductsReviewAdapter(getContext(), type);
        productsReview.setAdapter(storesAndProductsReviewAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        productsReview.setLayoutManager(linearLayoutManager);
    }
    private void initViews(View view) {
        productsReview = view.findViewById(R.id.productsReview);
    }
}
