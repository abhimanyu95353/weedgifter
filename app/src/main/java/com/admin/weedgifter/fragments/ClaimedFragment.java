package com.admin.weedgifter.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.ReceivedGiftsAdapter;

public class ClaimedFragment extends Fragment {
    private RecyclerView claimedView;
    private ReceivedGiftsAdapter seeMenuAdapter;
    private String viewType = "claimed";

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_claimed, container, false);
        initViews(root);
        setReceivedGiftList();
        return root;
    }
    private void setReceivedGiftList() {
        seeMenuAdapter = new ReceivedGiftsAdapter(getContext(),viewType);
        claimedView.setAdapter(seeMenuAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        claimedView.setLayoutManager(linearLayoutManager);
        seeMenuAdapter.notifyDataSetChanged();
    }
    private void initViews(View view) {
        claimedView = view.findViewById(R.id.claimedView);
    }
}
