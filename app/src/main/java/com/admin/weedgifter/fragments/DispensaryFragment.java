package com.admin.weedgifter.fragments;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.admin.weedgifter.R;

public class DispensaryFragment extends Fragment {
    private TextView text_gallery;

    public View onCreateView(@NonNull LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_dispensaries, container, false);
        text_gallery = root.findViewById(R.id.txt);
        return root;
    }
}