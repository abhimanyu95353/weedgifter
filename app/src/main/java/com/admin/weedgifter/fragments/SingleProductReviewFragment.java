package com.admin.weedgifter.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.activities.ReviewActiivty;
import com.admin.weedgifter.adapters.CommentsListAdapter;

import org.w3c.dom.Text;

public class SingleProductReviewFragment extends Fragment {
    private RecyclerView commentList;
    private TextView  writeReview;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_single_product_review, container, false);
        initViews(root);
        clickListners();
        setCommentList();
        return root;
    }
    private void clickListners() {
        writeReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (getContext(), ReviewActiivty.class);
                startActivity(intent);
            }
        });
    }
    private void setCommentList() {
        CommentsListAdapter commentsListAdapter = new CommentsListAdapter(getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        commentList.setAdapter(commentsListAdapter);
        commentList.setLayoutManager(linearLayoutManager);
        commentsListAdapter.notifyDataSetChanged();
    }
    private void initViews(View view) {
        commentList = view.findViewById(R.id.commentList);
        writeReview = view.findViewById(R.id.writeReview);
    }
}