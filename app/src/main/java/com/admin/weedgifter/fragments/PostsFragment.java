package com.admin.weedgifter.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.PostsSocialAdapter;
import com.admin.weedgifter.adapters.StoriesAdapter;

public class PostsFragment extends Fragment {
    private RecyclerView postsView, storiesView;
    private StoriesAdapter storiesAdapter;
    private PostsSocialAdapter postsAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_posts, container, false);
        initViews(root);
        setStoriesView();
        setPostsViews();
        return root;
    }
    private void setStoriesView() {
        storiesAdapter = new StoriesAdapter(getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        storiesView.setLayoutManager(linearLayoutManager);
        storiesView.setAdapter(storiesAdapter);
        storiesAdapter.notifyDataSetChanged();
    }
    private void setPostsViews() {
        postsAdapter = new PostsSocialAdapter(getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        postsView.setAdapter(postsAdapter);
        postsView.setLayoutManager(linearLayoutManager);
        postsAdapter.notifyDataSetChanged();
    }
    private void initViews(View view) {
        postsView = view.findViewById(R.id.postsView);
        storiesView = view.findViewById(R.id.storiesView);
    }
}
