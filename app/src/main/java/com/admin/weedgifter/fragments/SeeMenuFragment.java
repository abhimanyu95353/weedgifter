package com.admin.weedgifter.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.activities.ProductDetailActivity;
import com.admin.weedgifter.adapters.SeeMenuAdapter;
import com.admin.weedgifter.interfaces.StoreItemClick;

public class SeeMenuFragment extends Fragment implements StoreItemClick {
    private RecyclerView productsView;
    private SeeMenuAdapter seeMenuAdapter;
    private TextView allLabel, floweLabel, tropicaLabel, edibleLabel;
    private boolean isAll, isFlower, isTropica, isEdible;
    private StoreItemClick storeItemClick;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_see_menu, container, false);
        initViews(root);
        clickListners();
        setPrductsView();
        return root;
    }
    private void clickListners() {
        allLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isAll) {
                    allLabel.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_bg_selected));
                    isAll = true;
                } else {
                    allLabel.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_bg_unselected));
                    isAll = false;
                }
            }
        });
        floweLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFlower) {
                    floweLabel.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_bg_selected));
                    isFlower = true;
                } else {
                    floweLabel.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_bg_unselected));
                    isFlower = false;
                }
            }
        });
        tropicaLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isTropica) {
                    tropicaLabel.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_bg_selected));
                    isTropica = true;
                } else {
                    tropicaLabel.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_bg_unselected));
                    isTropica = false;
                }
            }
        });
        edibleLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isEdible) {
                    edibleLabel.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_bg_selected));
                    isEdible = true;
                } else {
                    edibleLabel.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_bg_unselected));
                    isEdible = false;
                }
            }
        });
    }
    private void setPrductsView() {
        seeMenuAdapter = new SeeMenuAdapter(getContext(), this);
        productsView.setAdapter(seeMenuAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        productsView.setLayoutManager(linearLayoutManager);
        seeMenuAdapter.notifyDataSetChanged();
    }
    private void initViews(View view) {
        productsView = view.findViewById(R.id.productsView);
        allLabel = view.findViewById(R.id.allLabel);
        floweLabel = view.findViewById(R.id.flowerLabel);
        tropicaLabel = view.findViewById(R.id.tropicaLabel);
        edibleLabel = view.findViewById(R.id.edibleLabel);
    }
    @Override
    public void onItemClick(String name) {
        Intent intent = new Intent(getContext(), ProductDetailActivity.class);
        startActivity(intent);
    }
}
