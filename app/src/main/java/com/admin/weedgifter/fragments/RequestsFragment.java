package com.admin.weedgifter.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.FriendRequestsListAdapter;
import com.admin.weedgifter.adapters.PeopleYouMayKnowAdapter;

public class RequestsFragment extends Fragment {
    private RecyclerView peopleYouMayKnowView, requestsView;
    private FriendRequestsListAdapter friendRequestsListAdapter;
    private PeopleYouMayKnowAdapter peopleYouMayKnowAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_requests, container, false);
        initViews(root);
        setFriendRequestView();
        setFriendSuggestionView();
        return root;
    }
    private void setFriendSuggestionView() {
        peopleYouMayKnowAdapter = new PeopleYouMayKnowAdapter(getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
        peopleYouMayKnowView.setAdapter(peopleYouMayKnowAdapter);
        peopleYouMayKnowView.setLayoutManager(linearLayoutManager);
        peopleYouMayKnowAdapter.notifyDataSetChanged();
    }
    private void setFriendRequestView() {
        friendRequestsListAdapter = new FriendRequestsListAdapter(getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
        requestsView.setAdapter(friendRequestsListAdapter);
        requestsView.setLayoutManager(linearLayoutManager);
        friendRequestsListAdapter.notifyDataSetChanged();
    }
    private void initViews(View view) {
        peopleYouMayKnowView = view.findViewById(R.id.peopleYouMayKnowView);
        requestsView = view.findViewById(R.id.requestsView);
    }
}
