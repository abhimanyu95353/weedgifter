package com.admin.weedgifter.fragments;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.activities.PostDetailActivity;
import com.admin.weedgifter.adapters.YourLastPhotosListAdapter;
import com.admin.weedgifter.utils.Common;
import com.admin.weedgifter.utils.Dialog;

import static android.app.Activity.RESULT_OK;
public class TimelineFragment extends Fragment {
    private EditText etPost;
    private LinearLayout addImageVideoLayout;
    private String postText;
    private Button btnPost;
    private View v1, v2;
    private RecyclerView lastPhotosView;
    private YourLastPhotosListAdapter yourLastPhotosListAdapter;
    private ImageView galleryIcon, videoIcon, capturedImage;
    private TextView tvAddPhotos, tvAddVideo;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private AlertDialog alertDialog;
    static final int RESULT_LOAD_IMAGE = 10;
    static final int REQUEST_VIDEO_CAPTURE = 2;
    static final int SELECT_VIDEO = 3;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_timeline, container, false);
        initViews(root);
        lastPhotosView();
        clickListners();
        return root;
    }
    private void clickListners() {
        tvAddPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImageDialog("photosDialog");
            }
        });
        tvAddVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImageDialog("videoDialog");

            }
        });
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), PostDetailActivity.class);
                startActivity(intent);
            }
        });
    }
    private void initViews(View view) {
        etPost = view.findViewById(R.id.etPost);
        btnPost = view.findViewById(R.id.btnPost);
        addImageVideoLayout = view.findViewById(R.id.addImageVideoLayout);
        v1 = view.findViewById(R.id.v1);
        v2 = view.findViewById(R.id.v2);
        galleryIcon = view.findViewById(R.id.galleryIcon);
        videoIcon = view.findViewById(R.id.videoIcon);
        tvAddPhotos = view.findViewById(R.id.tvAddPhotos);
        tvAddVideo = view.findViewById(R.id.tvAddVideo);
        capturedImage = view.findViewById(R.id.capturedImage);
        lastPhotosView = view.findViewById(R.id.lastPhotosView);
        etPost.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etPost.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.edit_text_textarea_bg_selected));
                    addImageVideoLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.edit_text_textarea_bottom_selected_bg));
                    v1.setBackgroundColor(Color.parseColor("#0D9B6B"));
                    v2.setBackgroundColor(Color.parseColor("#0D9B6B"));
                    tvAddPhotos.setTextColor(Color.WHITE);
                    tvAddVideo.setTextColor(Color.WHITE);
                    galleryIcon.setColorFilter(ContextCompat.getColor(getContext(), R.color.white), PorterDuff.Mode.SRC_IN);
                    videoIcon.setColorFilter(ContextCompat.getColor(getContext(), R.color.white), PorterDuff.Mode.SRC_IN);
                } else {
                    postText = etPost.getText().toString().trim();
                    if (postText.isEmpty()) {
                        etPost.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.edit_text_textarea_bg_unselected));
                        addImageVideoLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.edit_text_textarea_bottom_bg));
                        v1.setBackgroundColor(Color.parseColor("#4f4f4f"));
                        v2.setBackgroundColor(Color.parseColor("#4f4f4f"));
                        tvAddPhotos.setTextColor(Color.parseColor("#4f4f4f"));
                        tvAddVideo.setTextColor(Color.parseColor("#4f4f4f"));
                        galleryIcon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorUnselected), PorterDuff.Mode.SRC_IN);
                        videoIcon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorUnselected), PorterDuff.Mode.SRC_IN);
                        return;
                    } else {
                        Common.hideKeyboard(getContext(), etPost);
                    }
                }
            }
        });
    }
    private void lastPhotosView() {
        yourLastPhotosListAdapter = new YourLastPhotosListAdapter(getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        lastPhotosView.setAdapter(yourLastPhotosListAdapter);
        lastPhotosView.setLayoutManager(linearLayoutManager);
        yourLastPhotosListAdapter.notifyDataSetChanged();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            capturedImage.setVisibility(View.VISIBLE);
            capturedImage.setImageBitmap(imageBitmap);
            alertDialog.dismiss();
        } else if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK) {
            if (data != null) {
                alertDialog.dismiss();
                Common.displayToast(getContext(),"Photos Mapped Success.");
            }
        } else if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
            if (data != null) {
                alertDialog.dismiss();
                Common.displayToast(getContext(),"Video Mapped Success.");
            }
        } else if (requestCode == SELECT_VIDEO && resultCode == RESULT_OK) {
            if (data != null) {
                alertDialog.dismiss();
                Common.displayToast(getContext(),"Video from gallery mapped success.");
            }
        }
    }
    private void uploadImageDialog(String dialogType) {
        if (dialogType.equals("photosDialog")) {
            AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.dialog_upload_video_image, null);
            alert.setView(mView);
            alertDialog = alert.create();
            alertDialog.setCanceledOnTouchOutside(false);
            TextView tvGallery = mView.findViewById(R.id.tvGallery);
            TextView tvCamera = mView.findViewById(R.id.tvCamera);
            TextView titleText = mView.findViewById(R.id.titleText);
            TextView textInfo = mView.findViewById(R.id.textInfo);
            titleText.setText(getText(R.string.upload_image));
            textInfo.setText(getText(R.string.take_a_new_photo_or_select_one_from_your_photo_gallery));
            tvCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
            });
            tvGallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, RESULT_LOAD_IMAGE);
                }
            });
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        } else if (dialogType.equals("videoDialog")) {
            AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.dialog_upload_video_image, null);
            alert.setView(mView);
            alertDialog = alert.create();
            alertDialog.setCanceledOnTouchOutside(false);
            TextView titleText = mView.findViewById(R.id.titleText);
            TextView textInfo = mView.findViewById(R.id.textInfo);
            titleText.setText(getText(R.string.upload_video));
            textInfo.setText(getText(R.string.make_new_video));
            TextView tvGallery = mView.findViewById(R.id.tvGallery);
            TextView tvCamera = mView.findViewById(R.id.tvCamera);
            tvCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    if (takeVideoIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
                    }
                }
            });
            tvGallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, SELECT_VIDEO);
                }
            });
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        }
    }
}
