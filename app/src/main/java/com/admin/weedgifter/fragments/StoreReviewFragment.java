package com.admin.weedgifter.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.StoresAndProductsReviewAdapter;

public class StoreReviewFragment extends Fragment {
    private RecyclerView storesReview;
    private StoresAndProductsReviewAdapter storesAndProductsReviewAdapter;
    String type = "review";

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_stores_review, container, false);
        initViews(root);
        setStoreReviewList();
        return root;
    }
    private void setStoreReviewList() {
        storesAndProductsReviewAdapter = new StoresAndProductsReviewAdapter(getContext(), type);
        storesReview.setAdapter(storesAndProductsReviewAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        storesReview.setLayoutManager(linearLayoutManager);
    }
    private void initViews(View view) {
        storesReview = view.findViewById(R.id.storesReview);
    }
}
