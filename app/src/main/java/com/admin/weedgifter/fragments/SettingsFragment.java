package com.admin.weedgifter.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.admin.weedgifter.activities.MainActivity;
import com.admin.weedgifter.R;
import com.admin.weedgifter.activities.AccountActivity;
import com.admin.weedgifter.activities.ChangePasswordActivity;
import com.admin.weedgifter.activities.EditProfileActivity;
import com.admin.weedgifter.activities.MyFriendsActivity;
import com.admin.weedgifter.activities.MyReviewsActivity;
import com.admin.weedgifter.activities.NotificationsActivity;
import com.admin.weedgifter.activities.RecentGiftsActivity;
import com.admin.weedgifter.activities.TermsAndConditionsActivity;

public class SettingsFragment extends Fragment {
    private LinearLayout friendsLayout, recentGiftsLayout ;
    private RelativeLayout myAccount,logoutLayout, notificationsLayout,myReviewsLayout, termsLayout,giftlistLayout,changePasswordLayout;
    private ImageView editIcon;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_settings, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setLogo(null);
        initViews(root);
        clickListners();
        return root;
    }
    private void clickListners() {
        friendsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), MyFriendsActivity.class);
                startActivity(intent);
            }
        });
        recentGiftsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), RecentGiftsActivity.class);
                startActivity(intent);
            }
        });
        myAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AccountActivity.class);
                startActivity(intent);
            }
        });
        editIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), EditProfileActivity.class);
                startActivity(intent);
            }
        });
        notificationsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), NotificationsActivity.class);
                startActivity(intent);
            }
        });
        giftlistLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), RecentGiftsActivity.class);
                startActivity(intent);
            }
        });
        termsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), TermsAndConditionsActivity.class);
                startActivity(intent);
            }
        });
        myReviewsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), MyReviewsActivity.class);
                startActivity(intent);
            }
        });
        changePasswordLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ChangePasswordActivity.class);
                startActivity(intent);
            }
        });
        logoutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }
    private void initViews(View view) {
        friendsLayout = view.findViewById(R.id.friendsLayout);
        recentGiftsLayout = view.findViewById(R.id.recentGiftsLayout);
        myAccount = view.findViewById(R.id.myAccount);
        editIcon = view.findViewById(R.id.editIcon);
        notificationsLayout = view.findViewById(R.id.notificationsLayout);
        giftlistLayout = view.findViewById(R.id.giftlistLayout);
        termsLayout = view.findViewById(R.id.termsLayout);
        myReviewsLayout = view.findViewById(R.id.myReviewsLayout);
        changePasswordLayout = view.findViewById(R.id.changePasswordLayout);
        logoutLayout = view.findViewById(R.id.logoutLayout);
    }
}
