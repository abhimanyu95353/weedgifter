package com.admin.weedgifter.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.ReceivedGiftsAdapter;

public class FavouritesFragment extends Fragment {
    private RecyclerView favouritesView;
    private TextView storesFilter, itemFilter;
    private ReceivedGiftsAdapter seeMenuAdapter;
    private String viewType = "favourites";

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_favourites, container, false);
        initViews(root);
        clickListners();
        setFavouritesList();
        return root;
    }
    private void clickListners() {
        storesFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storesFilter.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.selected_store_filter_bg));
                itemFilter.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.unselected_item_filter_bg));
            }
        });
        itemFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemFilter.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.selected_item_filter_bg));
                storesFilter.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.unselected_store_filter_bg));
            }
        });
    }
    private void setFavouritesList() {
        seeMenuAdapter = new ReceivedGiftsAdapter(getContext(), viewType);
        favouritesView.setAdapter(seeMenuAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        favouritesView.setLayoutManager(linearLayoutManager);
        seeMenuAdapter.notifyDataSetChanged();
    }
    private void initViews(View view) {
        favouritesView = view.findViewById(R.id.favouritesView);
        storesFilter = view.findViewById(R.id.storesFilter);
        itemFilter = view.findViewById(R.id.itemFilter);
    }
}
