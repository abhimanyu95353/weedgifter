package com.admin.weedgifter.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.ReceivedGiftsAdapter;
import com.admin.weedgifter.adapters.SeeMenuAdapter;

public class ReceivedFragment extends Fragment {
    private RecyclerView receivedView;
    private ReceivedGiftsAdapter seeMenuAdapter;
    private String viewType ="received";

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_received, container, false);
        initViews(root);
        setReceivedGiftList();
        return root;
    }
    private void setReceivedGiftList() {
        seeMenuAdapter = new ReceivedGiftsAdapter(getContext(),viewType);
        receivedView.setAdapter(seeMenuAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        receivedView.setLayoutManager(linearLayoutManager);
        seeMenuAdapter.notifyDataSetChanged();
    }
    private void initViews(View view) {
        receivedView = view.findViewById(R.id.receivedView);
    }
}
