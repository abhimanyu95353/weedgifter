package com.admin.weedgifter.interfaces;

public interface StoreItemClickAsynResult<TData> {
     void success(TData data);
     void error(String error);
}
