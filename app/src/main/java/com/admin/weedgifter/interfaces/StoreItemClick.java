package com.admin.weedgifter.interfaces;

public interface StoreItemClick {
    void onItemClick(String name);
}
