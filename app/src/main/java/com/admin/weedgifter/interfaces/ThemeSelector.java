package com.admin.weedgifter.interfaces;

public interface ThemeSelector {
    void onThemeSelected(String themeName);
}
