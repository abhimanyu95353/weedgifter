package com.admin.weedgifter.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.activities.AccountSettingsActivity;

import java.time.Instant;

public class AccountsListAdapter extends RecyclerView.Adapter<AccountsListAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private Context context;
    private String layout;

    public AccountsListAdapter(Context context, String layout) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.layout = layout;
    }
    @NonNull
    @Override
    public AccountsListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layout.equals("accountList")) {
            View view = mInflater.inflate(R.layout.accounts_view_item, parent, false);
            return new AccountsListAdapter.ViewHolder(view);
        } else {
            View view = mInflater.inflate(R.layout.bank_view_item, parent, false);
            return new AccountsListAdapter.ViewHolder(view);
        }
    }
    @Override
    public void onBindViewHolder(@NonNull AccountsListAdapter.ViewHolder holder, int position) {
        holder.editAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AccountSettingsActivity.class);
                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return 2;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView editAccount;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            editAccount = itemView.findViewById(R.id.editAccount);
        }
    }
}
