package com.admin.weedgifter.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.activities.StoresActivity;
import com.admin.weedgifter.interfaces.StoreItemClick;

public class ReceivedGiftsAdapter extends RecyclerView.Adapter<ReceivedGiftsAdapter.ViewHolder> {
    private Context context;
    private LayoutInflater mInflater;
    private String viewType;

    public ReceivedGiftsAdapter(Context context, String viewType) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.viewType = viewType;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.received_gift_item, parent, false);
        return new ReceivedGiftsAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        switch (viewType) {
            case "received":
                holder.verified.setVisibility(View.GONE);
                holder.like.setColorFilter(ContextCompat.getColor(context, R.color.white), PorterDuff.Mode.SRC_IN);
                break;
            case "claimed":
                holder.like.setColorFilter(ContextCompat.getColor(context, R.color.white), PorterDuff.Mode.SRC_IN);
                break;
            case "favourites":
                holder.verified.setVisibility(View.VISIBLE);
                holder.like.setColorFilter(ContextCompat.getColor(context, R.color.errorColor), PorterDuff.Mode.SRC_IN);
                break;
        }
    }
    @Override
    public int getItemCount() {
        return 3;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView verified, like;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            verified = itemView.findViewById(R.id.verified);
            like = itemView.findViewById(R.id.like);
        }
    }
}
