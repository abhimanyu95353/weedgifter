package com.admin.weedgifter.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;

public class CommentsPostAdapter extends RecyclerView.Adapter<CommentsPostAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private Context context;

    public CommentsPostAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }
    @NonNull
    @Override
    public CommentsPostAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.comments_posts_view_item, parent, false);
        return new CommentsPostAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull CommentsPostAdapter.ViewHolder holder, int position) { }
    @Override
    public int getItemCount() {
        return 7;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
