package com.admin.weedgifter.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;

public class StoresAndProductsReviewAdapter extends RecyclerView.Adapter<StoresAndProductsReviewAdapter.ViewHolder> {
    private Context context;
    private LayoutInflater mInflater;
    private String viewType;

    public StoresAndProductsReviewAdapter(Context context, String viewType) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.viewType = viewType;
    }
    @NonNull
    @Override
    public StoresAndProductsReviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.stores_products_review_item, parent, false);
        return new StoresAndProductsReviewAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull StoresAndProductsReviewAdapter.ViewHolder holder, int position) {
        switch (viewType) {
            case "review":
                holder.price.setVisibility(View.GONE);
                break;
            case "products":
                holder.price.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView verified, like;
        private TextView price;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            price = itemView.findViewById(R.id.price);
        }
    }
}

