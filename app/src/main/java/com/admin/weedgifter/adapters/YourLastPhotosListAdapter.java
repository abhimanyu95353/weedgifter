package com.admin.weedgifter.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.activities.CommentsSocialActivity;
import com.admin.weedgifter.activities.PostDetailActivity;
import com.admin.weedgifter.utils.Dialog;

public class YourLastPhotosListAdapter extends RecyclerView.Adapter<YourLastPhotosListAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private Context context;

    public YourLastPhotosListAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }
    @NonNull
    @Override
    public YourLastPhotosListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.last_posts_view_item, parent, false);
        return new YourLastPhotosListAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull YourLastPhotosListAdapter.ViewHolder holder, int position) {
        holder.postItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PostDetailActivity.class);
                context.startActivity(intent);
            }
        });
        holder.likedList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog.likedPeopleDialog(context);
            }
        });
        holder.commentsList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CommentsSocialActivity.class);
                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return 3;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout postItem;
        private LinearLayout likedList, commentsList;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            postItem = itemView.findViewById(R.id.postItem);
            likedList = itemView.findViewById(R.id.likedList);
            commentsList = itemView.findViewById(R.id.commentsList);
        }
    }
}
