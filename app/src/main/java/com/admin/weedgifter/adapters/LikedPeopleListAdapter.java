package com.admin.weedgifter.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.activities.AccountSettingsActivity;

public class LikedPeopleListAdapter extends RecyclerView.Adapter<LikedPeopleListAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private Context context;

    public LikedPeopleListAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }
    @NonNull
    @Override
    public LikedPeopleListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.liked_people_view_item, parent, false);
        return new LikedPeopleListAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull LikedPeopleListAdapter.ViewHolder holder, int position) {}
    @Override
    public int getItemCount() {
        return 5;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
