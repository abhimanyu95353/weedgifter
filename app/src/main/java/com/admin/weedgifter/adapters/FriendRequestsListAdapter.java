package com.admin.weedgifter.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.activities.AccountSettingsActivity;

public class FriendRequestsListAdapter extends RecyclerView.Adapter<FriendRequestsListAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private Context context;

    public FriendRequestsListAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }
    @NonNull
    @Override
    public FriendRequestsListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.friend_request_view_item, parent, false);
            return new FriendRequestsListAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull FriendRequestsListAdapter.ViewHolder holder, int position) {}
    @Override
    public int getItemCount() {
        return 2;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
