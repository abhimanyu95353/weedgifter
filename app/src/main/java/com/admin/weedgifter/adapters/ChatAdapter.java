package com.admin.weedgifter.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.activities.AccountSettingsActivity;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private Context context;

    public ChatAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }
    @NonNull
    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.chat_view_item, parent, false);
        return new ChatAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ChatAdapter.ViewHolder holder, int position) { }
    @Override
    public int getItemCount() {
        return 20;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
 private LinearLayout senderLayout,receiverLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            senderLayout = itemView.findViewById(R.id.senderLayout);
            receiverLayout = itemView.findViewById(R.id.receiverLayout);
        }
    }
}
