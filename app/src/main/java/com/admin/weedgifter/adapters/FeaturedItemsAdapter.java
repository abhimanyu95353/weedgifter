package com.admin.weedgifter.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;

public class FeaturedItemsAdapter extends RecyclerView.Adapter<FeaturedItemsAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private Context context;
    private String layoutType;

    public FeaturedItemsAdapter(Context context, String layoutType) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.layoutType = layoutType;
    }
    @NonNull
    @Override
    public FeaturedItemsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (layoutType) {
            case "featuredView":
            case "deliveriesView":
            case "merchandiseView":
            case "storefrontView":{
                View view = mInflater.inflate(R.layout.featured_item, parent, false);
                return new ViewHolder(view);
            }
            case "doctorsView": {
                View view = mInflater.inflate(R.layout.doctors_view_item, parent, false);
                return new ViewHolder(view);
            }
//            case "storefrontView": {
//                View view = mInflater.inflate(R.layout.storefront_view_item, parent, false);
//                return new ViewHolder(view);
//            }
        }
        return null;
    }
    @Override
    public void onBindViewHolder(@NonNull FeaturedItemsAdapter.ViewHolder holder, int position) {
        holder.itemName.setText("Stanzy");
    }
    @Override
    public int getItemCount() {
        return 7;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView itemName;
        private ImageView itemImage;
        private RatingBar ratingBar;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.itemName);
            itemImage = itemView.findViewById(R.id.itemImage);
            ratingBar = itemView.findViewById(R.id.ratingBar);
        }
    }
}
