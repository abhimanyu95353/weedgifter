package com.admin.weedgifter.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.activities.CommentsSocialActivity;
import com.admin.weedgifter.activities.PostDetailActivity;
import com.admin.weedgifter.utils.Dialog;

public class PostsSocialAdapter extends RecyclerView.Adapter<PostsSocialAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private Context context;

    public PostsSocialAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }
    @NonNull
    @Override
    public PostsSocialAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.posts_view_item, parent, false);
        return new PostsSocialAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull PostsSocialAdapter.ViewHolder holder, int position) {
        holder.postItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PostDetailActivity.class);
                context.startActivity(intent);
            }
        });
        holder.likedList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog.likedPeopleDialog(context);
            }
        });
        holder.commentsList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CommentsSocialActivity.class);
                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return 10;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout postItem;
        private LinearLayout likedList, commentsList;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            postItem = itemView.findViewById(R.id.postItem);
            likedList = itemView.findViewById(R.id.likedList);
            commentsList = itemView.findViewById(R.id.commentsList);
        }
    }
}
