package com.admin.weedgifter.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.interfaces.StoreItemClickAsynResult;

import java.util.ArrayList;

public class StoresListAdapter extends RecyclerView.Adapter<StoresListAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private Context context;
    private ArrayList<String> list;
    private StoreItemClickAsynResult<String> storeItemClickAsynResult;
    public StoresListAdapter(Context context, ArrayList<String> list, StoreItemClickAsynResult<String> storeItemClickAsynResult) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.list=list;
        this.storeItemClickAsynResult = storeItemClickAsynResult;
    }
    @NonNull
    @Override
    public StoresListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.stores_view_item, parent, false);
        return new StoresListAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull StoresListAdapter.ViewHolder holder, int position) {
        holder.parentContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               storeItemClickAsynResult.success("item clicked");
            }
        });
    }
    @Override
    public int getItemCount() {
        return 7;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout parentContainer;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            parentContainer = itemView.findViewById(R.id.parentContainer);
        }
    }
}
