package com.admin.weedgifter.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.activities.ChatActivity;

public class InboxListAdapter extends RecyclerView.Adapter<InboxListAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private Context context;

    public InboxListAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }
    @NonNull
    @Override
    public InboxListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.inbox_view_item, parent, false);
        return new InboxListAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull InboxListAdapter.ViewHolder holder, int position) {
        holder.tvSendMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChatActivity.class);
                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return 5;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvSendMsg;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSendMsg = itemView.findViewById(R.id.tvSendMsg);
        }
    }
}
