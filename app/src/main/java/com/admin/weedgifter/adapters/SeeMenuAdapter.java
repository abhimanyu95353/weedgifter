package com.admin.weedgifter.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.interfaces.StoreItemClick;

public class SeeMenuAdapter extends RecyclerView.Adapter<SeeMenuAdapter.ViewHolder> {
    private Context context;
    private LayoutInflater mInflater;
    private StoreItemClick storeItemClick;

    public SeeMenuAdapter(Context context,StoreItemClick storeItemClick) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.storeItemClick = storeItemClick;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.see_menu_item, parent, false);
        return new SeeMenuAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.parentContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storeItemClick.onItemClick("Item clicked.");
            }
        });
    }
    @Override
    public int getItemCount() {
        return 5;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout parentContainer;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            parentContainer = itemView.findViewById(R.id.parentContainer);
        }
    }
}
