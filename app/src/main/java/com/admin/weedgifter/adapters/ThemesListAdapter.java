package com.admin.weedgifter.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.activities.AccountSettingsActivity;
import com.admin.weedgifter.interfaces.ThemeSelector;

public class ThemesListAdapter extends RecyclerView.Adapter<ThemesListAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private Context context;
    private ThemeSelector themeSelector;

    public ThemesListAdapter(Context context, ThemeSelector themeSelector) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.themeSelector = themeSelector;
    }
    @NonNull
    @Override
    public ThemesListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.theme_view_item, parent, false);
        return new ThemesListAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ThemesListAdapter.ViewHolder holder, final int position) {
        holder.church.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                themeSelector.onThemeSelected("churchTheme");
            }
        });
        holder.party.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                themeSelector.onThemeSelected("partyTheme");
            }
        });
        holder.globe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                themeSelector.onThemeSelected("globeTheme");
            }
        });
        holder.yellowish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                themeSelector.onThemeSelected("yellowishTheme");
            }
        });
    }
    @Override
    public int getItemCount() {
        return 1;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView church, party, globe, yellowish;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            church = itemView.findViewById(R.id.church);
            party = itemView.findViewById(R.id.party);
            globe = itemView.findViewById(R.id.globe);
            yellowish = itemView.findViewById(R.id.yellowish);
        }
    }
}