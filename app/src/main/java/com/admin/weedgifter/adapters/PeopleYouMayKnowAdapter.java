package com.admin.weedgifter.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;

public class PeopleYouMayKnowAdapter extends RecyclerView.Adapter<PeopleYouMayKnowAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private Context context;

    public PeopleYouMayKnowAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }
    @NonNull
    @Override
    public PeopleYouMayKnowAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.people_u_may_know_item, parent, false);
            return new PeopleYouMayKnowAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull PeopleYouMayKnowAdapter.ViewHolder holder, int position) {}
    @Override
    public int getItemCount() {
        return 4;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
