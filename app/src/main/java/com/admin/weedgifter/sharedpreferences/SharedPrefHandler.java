package com.admin.weedgifter.sharedpreferences;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

public class SharedPrefHandler {
    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences("context", Context.MODE_PRIVATE);
    }
    public static void setStringValues(Context context, String key, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(key, data);
		editor.apply();
    }
    public static String getStringValues(Context context, String key) {
        return getSharedPreferences(context).getString(key, "");
    }
    public static void setObjectValues(Context ctx,String key,Object obj){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        editor.putString(key, json);
        editor.apply();
    }
//    public static UserLoginBean.DataBean getObjectValues(Context ctx, String key){
//        SharedPreferences preferences = getSharedPreferences(ctx);
//        Gson gson = new Gson();
//        String json = preferences.getString(key, "");
//        UserLoginBean.DataBean data = gson.fromJson(json, UserLoginBean.DataBean.class);
//        return data;
//    }
    public static void setBooleanValues(Context context, String key, Boolean data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(key, data);
        editor.apply();
    }
    public static boolean getBooleanValues(Context context, String key) {
        return getSharedPreferences(context).getBoolean(key, false);
    }
    public static long getLongValues(Context context, String key) {
        return getSharedPreferences(context).getLong(key, 0L);
    }
    public static void setLongValues(Context context, String key, Long DataToSave) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putLong(key, DataToSave);
        editor.apply();
    }
//    public void saveArrayList(ArrayList<String> list, String key){
//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
//        SharedPreferences.Editor editor = prefs.edit();
//        Gson gson = new Gson();
//        String json = gson.toJson(list);
//        editor.putString(key, json);
//        editor.apply();     // This line is IMPORTANT !!!
//    }
//
//    public ArrayList<String> getArrayList(String key){
//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
//        Gson gson = new Gson();
//        String json = prefs.getString(key, null);
//        Type type = new TypeToken<ArrayList<String>>() {}.getType();
//        return gson.fromJson(json, type);
//    }
}

