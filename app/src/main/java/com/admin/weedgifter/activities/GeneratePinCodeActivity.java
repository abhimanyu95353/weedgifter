package com.admin.weedgifter.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import com.admin.weedgifter.R;
import com.chaos.view.PinView;

public class GeneratePinCodeActivity extends AppCompatActivity {
    private ImageView close;
    private PinView pinView;
    private TextView errorText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_pin_code);
        initViews();
        clickListners();
    }
    private void clickListners() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void initViews() {
        close = findViewById(R.id.close);
        errorText = findViewById(R.id.errorText);
        pinView = findViewById(R.id.pinView);
        close = findViewById(R.id.close);
        pinView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    pinView.setLineColor(Color.parseColor("#A7A7A7"));
                    pinView.setItemBackgroundColor(Color.parseColor("#00000000"));
                }
            }
        });
        pinView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || event != null && event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    if (event == null || !event.isShiftPressed()) {
                        // the user is done typing.
                        String code = pinView.getText().toString();
                        if (code.equals("")) {
                            pinView.setLineColor(ResourcesCompat.getColor(getResources(), R.color.errorColor, getTheme()));
                            errorText.setVisibility(View.VISIBLE);
                            errorText.setText(getText(R.string.fill_all_fields));
                        } else if (code.length() < 4) {
                            pinView.setLineColor(ResourcesCompat.getColor(getResources(), R.color.errorColor, getTheme()));
                            errorText.setVisibility(View.VISIBLE);
                            errorText.setText(getText(R.string.fill_all_fields));
                        } else if (!code.equals("1234")) {
                            errorText.setVisibility(View.VISIBLE);
                            errorText.setText(getText(R.string.enter_correct_code));
                        } else if (code.equals("1234")) {
                            errorText.setVisibility(View.GONE);
                            pinView.setLineColor(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, getTheme()));
                            Intent intent = new Intent(GeneratePinCodeActivity.this, PinGenerateSuccessfulActivity.class);
                            startActivity(intent);
                        }
                        return  false;
                    }
                }
                return false;
            }
        });
    }
}
