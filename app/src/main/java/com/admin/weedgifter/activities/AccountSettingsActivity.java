package com.admin.weedgifter.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.admin.weedgifter.R;
import com.admin.weedgifter.utils.Common;
import com.admin.weedgifter.utils.Dialog;

public class AccountSettingsActivity extends AppCompatActivity {
    private EditText etCardNumber, etCvv, etCardHolderName, etExpiration;
    private String cardNumber, expiration, cvv, cardHolderName;
    private Button btnSave;
    private ImageView close;
    private TextView errorText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings);
        initViews();
    }
    private void initViews() {
        etCardNumber = findViewById(R.id.etCardNumber);
        close = findViewById(R.id.close);
        etCvv = findViewById(R.id.etCvv);
        etCardHolderName = findViewById(R.id.etCardHolderName);
        etExpiration = findViewById(R.id.etExpiration);
        errorText = findViewById(R.id.errorText);
        btnSave = findViewById(R.id.btnSave);
        etCardNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etCardNumber.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    cardNumber = etCardNumber.getText().toString().trim();
                    if (cardNumber.isEmpty()) {
                        return;
                    } else if (!Common.isCardNumberValid(cardNumber)) {
                        etCardNumber.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.error_bg));
                        errorText.setVisibility(View.VISIBLE);
                        errorText.setText(getText(R.string.card_number_limit));
                    } else {
                        etCardNumber.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                    }
                }
            }
        });
        etExpiration.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etExpiration.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    expiration = etExpiration.getText().toString().trim();
                    if (expiration.isEmpty()) {
                        return;
                    } else {
                        etExpiration.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.validated_fields));
                    }
                }
            }
        });
        etCvv.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etCvv.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    cvv = etCvv.getText().toString().trim();
                    if (cvv.isEmpty()) {
                        return;
                    } else if (!Common.isCvvValid(cvv)) {
                        errorText.setVisibility(View.VISIBLE);
                        errorText.setText(getText(R.string.enter_valid_cvv));
                        etCvv.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.error_bg));
                    } else {
                        errorText.setVisibility(View.GONE);
                        etCvv.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.validated_fields));
                    }
                }
            }
        });
        etCardHolderName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etCardHolderName.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    cardHolderName = etCardHolderName.getText().toString().trim();
                    if (cardHolderName.equals("")) {
                        return;
                    } else {
                        etCardHolderName.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.validated_fields));
                        Common.hideKeyboard(AccountSettingsActivity.this, view);
                    }
                }
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardNumber = etCardNumber.getText().toString().trim();
                expiration = etExpiration.getText().toString().trim();
                cvv = etCvv.getText().toString().trim();
                cardHolderName = etCardHolderName.getText().toString().trim();
                if (cardNumber.equals("") && expiration.equals("") && cvv.equals("") && cardHolderName.equals("")) {
                    etCardNumber.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.error_bg));
                    etExpiration.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.error_bg));
                    etCvv.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.error_bg));
                    etCardHolderName.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.error_bg));
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(getText(R.string.fill_all_fields));
                } else if (cardNumber.equals("")) {
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(getText(R.string.fill_all_fields));
                    etCardNumber.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.error_bg));
                } else if (expiration.equals("")) {
                    etExpiration.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.error_bg));
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(getText(R.string.fill_all_fields));
                } else if (cvv.equals("")) {
                    etCvv.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.error_bg));
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(getText(R.string.fill_all_fields));
                } else if (cardHolderName.equals("")) {
                    etCardHolderName.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.error_bg));
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(getText(R.string.fill_all_fields));
                } else if (!Common.isCardNumberValid(cardNumber)) {
                    etCardNumber.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.error_bg));
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(getText(R.string.card_number_limit));
                } else if (!Common.isCvvValid(cvv)) {
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(getText(R.string.enter_valid_cvv));
                    etCvv.setBackground(ContextCompat.getDrawable(AccountSettingsActivity.this, R.drawable.error_bg));
                } else {
                    Dialog.accountUdpatedDialog(AccountSettingsActivity.this);
                }
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
