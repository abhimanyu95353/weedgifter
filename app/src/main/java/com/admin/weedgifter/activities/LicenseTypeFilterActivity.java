package com.admin.weedgifter.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.admin.weedgifter.R;

public class LicenseTypeFilterActivity extends AppCompatActivity {
    private ImageView close;
    private CheckBox allChekbox, medicalCheckbox, recreationalCheckbox;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_license_type_filter);
        initViews();
        clickListner();
    }
    private void clickListner() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        allChekbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    allChekbox.setChecked(true);
                    medicalCheckbox.setChecked(true);
                    recreationalCheckbox.setChecked(true);
                }
            }
        });
    }
    private void initViews() {
        close = findViewById(R.id.close);
        allChekbox = findViewById(R.id.allCheckbox);
        medicalCheckbox = findViewById(R.id.medicalCheckbox);
        recreationalCheckbox = findViewById(R.id.recreationalCheckbox);
    }
}
