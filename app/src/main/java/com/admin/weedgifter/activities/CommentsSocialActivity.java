package com.admin.weedgifter.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.CommentsPostAdapter;
import com.admin.weedgifter.utils.Dialog;

public class CommentsSocialActivity extends AppCompatActivity {
    private RecyclerView commentsView;
    private CommentsPostAdapter commentsPostAdapter;
    private ImageView close, share;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments_social);
        initViews();
        clickListners();
        setCommentsView();
    }
    private void clickListners() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });}
    private void setCommentsView() {
        commentsPostAdapter = new CommentsPostAdapter(this);
        commentsView.setAdapter(commentsPostAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        commentsView.setLayoutManager(linearLayoutManager);
        commentsPostAdapter.notifyDataSetChanged();
    }
    private void initViews() {
        commentsView = findViewById(R.id.commentsView);
        close = findViewById(R.id.close);
        share = findViewById(R.id.share);
    }
}
