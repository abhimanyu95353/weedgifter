package com.admin.weedgifter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.admin.weedgifter.R;
import com.admin.weedgifter.utils.Common;
import com.chaos.view.PinView;

public class EnterPinActivity extends AppCompatActivity {
    private PinView pinView;
    private ImageView close;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_pin);
        close = findViewById(R.id.close);
        pinView = findViewById(R.id.pinView);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        pinView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || event != null &&
                        event.getAction() == KeyEvent.ACTION_DOWN &&
                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    if (event == null || !event.isShiftPressed()) {
                        // the user is done typing.
                        String code = pinView.getText().toString();
                        if (code.equals("")) {
                            Common.toastMsg(EnterPinActivity.this, getString(R.string.enter_pin));
                        } else if (code.length() < 4) {
                            Common.toastMsg(EnterPinActivity.this, getString(R.string.pin_limit));
                        } else if (!code.equals("1234")) {
                            Common.toastMsg(EnterPinActivity.this, getString(R.string.enter_correct_pin));
                        } else if (code.equals("1234")) {
                            Intent intent = new Intent(EnterPinActivity.this, GiftSendSuccessfulyActivity.class);
                            startActivity(intent);
                        }
                        return false;
                    }
                }
                return false;
            }
        });
    }
}

