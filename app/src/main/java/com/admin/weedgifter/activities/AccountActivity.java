package com.admin.weedgifter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.AccountsListAdapter;

public class AccountActivity extends AppCompatActivity {
    private RecyclerView accountList;
    private AccountsListAdapter accountsListAdapter;
    private LinearLayoutManager linearLayoutManager;
    private TextView addAccount;
    private ImageView close;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_account);
        initViews();
        setAccountsList();
    }
    private void setAccountsList() {
        accountsListAdapter = new AccountsListAdapter(AccountActivity.this,"accountList");
        accountList.setAdapter(accountsListAdapter);
        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        accountList.setLayoutManager(linearLayoutManager);
        accountsListAdapter.notifyDataSetChanged();
    }
    private void initViews() {
        accountList = findViewById(R.id.accountList);
        addAccount = findViewById(R.id.addAccount);
        close = findViewById(R.id.close);
        addAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AccountActivity.this, AddAccountActivity.class);
                startActivity(intent);
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
