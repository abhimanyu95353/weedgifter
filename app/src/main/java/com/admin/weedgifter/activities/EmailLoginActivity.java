package com.admin.weedgifter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.admin.weedgifter.R;
import com.admin.weedgifter.utils.Common;

public class EmailLoginActivity extends AppCompatActivity {
    private TextView signUp, errorText, forgotPassword;
    private ImageView close;
    private EditText etEmail, etPassword;
    private String email, password;
    private Button btnGetStarted;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_login);
        initViews();
        clickListners();
    }
    private void clickListners() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EmailLoginActivity.this, SignupActivity.class);
                startActivity(intent);
            }
        });
        btnGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSignInClick();

            }
        });
        etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etEmail.setBackground(ContextCompat.getDrawable(EmailLoginActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    email = etEmail.getText().toString().trim();
                    if (email.equals("")) {
                        return;
                    } else if (!Common.isEmailValid(email)) {
                        errorText.setVisibility(View.VISIBLE);
                        etEmail.setBackground(ContextCompat.getDrawable(EmailLoginActivity.this, R.drawable.error_bg));
                        errorText.setText(getText(R.string.enter_app_email));
                    } else {
                        etEmail.setBackground(ContextCompat.getDrawable(EmailLoginActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                    }
                }
            }
        });
        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etPassword.setBackground(ContextCompat.getDrawable(EmailLoginActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    Common.hideKeyboard(EmailLoginActivity.this, etPassword);
                    password = etPassword.getText().toString().trim();
                    if (password.equals("")) {
                        //noinspection SingleStatementInBlock
                        return;
                    } else if (!Common.isPasswordValid(password)) {
                        errorText.setVisibility(View.VISIBLE);
                        etPassword.setBackground(ContextCompat.getDrawable(EmailLoginActivity.this, R.drawable.error_bg));
                        errorText.setText(getText(R.string.password_valid));
                    } else {
                        etPassword.setBackground(ContextCompat.getDrawable(EmailLoginActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                        Common.hideKeyboard(EmailLoginActivity.this, view);
                        onSignInClick();
                    }
                }
            }
        });
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EmailLoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
    }
    private void onSignInClick() {
        email = etEmail.getText().toString().trim();
        password = etPassword.getText().toString().trim();
        if (email.equals("") && password.equals("")) {
            etEmail.setBackground(ContextCompat.getDrawable(EmailLoginActivity.this, R.drawable.error_bg));
            etPassword.setBackground(ContextCompat.getDrawable(EmailLoginActivity.this, R.drawable.error_bg));
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
        } else if (!(Common.isEmailValid(email))) {
            errorText.setVisibility(View.VISIBLE);
            etEmail.setBackground(ContextCompat.getDrawable(EmailLoginActivity.this, R.drawable.error_bg));
            errorText.setText(getText(R.string.enter_app_email));
        } else if (!(password.length() >= 8)) {
            errorText.setVisibility(View.VISIBLE);
            etPassword.setBackground(ContextCompat.getDrawable(EmailLoginActivity.this, R.drawable.error_bg));
            errorText.setText(getText(R.string.password_valid));
        } else {
            etEmail.setBackground(ContextCompat.getDrawable(EmailLoginActivity.this, R.drawable.validated_fields));
            etPassword.setBackground(ContextCompat.getDrawable(EmailLoginActivity.this, R.drawable.validated_fields));
            errorText.setVisibility(View.GONE);
            Common.hideKeyboard(this, btnGetStarted);
            Common.displayToast(this,getString(R.string.sign_in_success));
        }
    }
    private void initViews() {
        close = findViewById(R.id.close);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        signUp = findViewById(R.id.signUp);
        btnGetStarted = findViewById(R.id.btnGetStarted);
        errorText = findViewById(R.id.errorText);
        forgotPassword = findViewById(R.id.forgotPassword);
        String spanText = getText(R.string.dont_have_an_acc) + "REGISTER";
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(spanText);
        int flag = 0;
        ssBuilder.setSpan(new TextAppearanceSpan(this, R.style.SpanTextAppearance1), 0, spanText.length(), flag);
        signUp.setText((Html.fromHtml("<font color=\"#ffffff\">" + getText(R.string.new_to_weedmaps_sign_up) + "</font>" + "<font color=\"#0D9B6B\">" + " REGISTER" + "</font>")));
    }
}
