package com.admin.weedgifter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.admin.weedgifter.R;
import com.admin.weedgifter.utils.Common;

public class EmailSignupActivity extends AppCompatActivity {
    private TextView privacyText, signUp, passwordError, confirmPasswordError, errorText, emailError;
    private ImageView close;
    private String login, email, password, confirmPassword;
    private Button btnNext;
    private EditText etEmail, etPassword, etConfirmPassword;
    private CheckBox radioTerms;
    private LinearLayout checkboxContainer;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_signup);
        initViews();
        clickListners();
    }
    private void clickListners() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EmailSignupActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNextClick();
            }
        });
        radioTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    checkboxContainer.setBackgroundResource(R.drawable.checkbox_checked_validation);
                } else {
                    checkboxContainer.setBackgroundResource(R.drawable.checkbox_checked_validation);
                }
            }
        });
        privacyText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EmailSignupActivity.this, TermsAndConditionsActivity.class);
                startActivity(intent);
            }
        });
        etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etEmail.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    email = etEmail.getText().toString().trim();
                    if (email.equals("")) {
                        return;
                    } else if (!Common.isEmailValid(email)) {
                        errorText.setVisibility(View.VISIBLE);
                        etEmail.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.error_bg));
                        errorText.setText(getText(R.string.enter_app_email));
                    } else {
                        etEmail.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                    }
                }
            }
        });
        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etPassword.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    password = etPassword.getText().toString().trim();
                    if (password.equals("")) {
                        return;
                    } else if (!Common.isPasswordValid(password)) {
                        errorText.setVisibility(View.GONE);
                        passwordError.setVisibility(View.VISIBLE);
                        confirmPasswordError.setVisibility(View.GONE);
                        etPassword.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.error_bg));
                        passwordError.setText(getText(R.string.password_valid));
                    } else {
                        etPassword.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                        passwordError.setVisibility(View.GONE);

                    }
                }
            }
        });
        etConfirmPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etConfirmPassword.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    confirmPassword = etConfirmPassword.getText().toString().trim();
                    if (confirmPassword.equals("")) {
                        return;
                    } else if (!Common.isConfirmPassword(confirmPassword)) {
                        errorText.setVisibility(View.GONE);
                        confirmPasswordError.setVisibility(View.VISIBLE);
                        etConfirmPassword.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.error_bg));
                        confirmPasswordError.setText(getText(R.string.password_valid));
                    } else if (!confirmPassword.matches(password)) {
                        confirmPasswordError.setVisibility(View.GONE);
                        errorText.setVisibility(View.VISIBLE);
                        errorText.setText(getText(R.string.password_not_match));
                        Common.hideKeyboard(EmailSignupActivity.this, btnNext);
                    } else {
                        etConfirmPassword.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                        Common.hideKeyboard(EmailSignupActivity.this, btnNext);
                    }
                }
            }
        });
    }
    private void onNextClick() {
        email = etEmail.getText().toString().trim();
        password = etPassword.getText().toString().trim();
        confirmPassword = etConfirmPassword.getText().toString().trim();
        if (email.equals("") && password.equals("") && confirmPassword.equals("") && !(radioTerms.isChecked())) {
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
            etEmail.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.error_bg));
            etPassword.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.error_bg));
            etConfirmPassword.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.error_bg));
            checkboxContainer.setBackgroundResource(R.drawable.checkbox_unchecked_validation);
        } else if (!(radioTerms.isChecked())) {
            checkboxContainer.setBackgroundResource(R.drawable.checkbox_unchecked_validation);
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
        } else if (!Common.isEmailValid(email)) {
            errorText.setVisibility(View.GONE);
            emailError.setVisibility(View.VISIBLE);
            etEmail.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.error_bg));
            emailError.setText(getText(R.string.enter_app_email));
        } else if (!Common.isPasswordValid(password)) {
            errorText.setVisibility(View.GONE);
            passwordError.setVisibility(View.VISIBLE);
            etPassword.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.error_bg));
            passwordError.setText(getText(R.string.password_valid));
        } else if (!Common.isPasswordValid(confirmPassword)) {
            errorText.setVisibility(View.GONE);
            passwordError.setVisibility(View.GONE);
            etConfirmPassword.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.error_bg));
            confirmPasswordError.setVisibility(View.VISIBLE);
            confirmPasswordError.setText(getText(R.string.confirm_password_valid));
        } else if (!password.matches(confirmPassword)) {
            Common.hideKeyboard(this, btnNext);
            errorText.setVisibility(View.VISIBLE);
            confirmPasswordError.setVisibility(View.GONE);
            passwordError.setVisibility(View.GONE);
            errorText.setText(getText(R.string.password_not_match));
        } else {
            emailError.setVisibility(View.GONE);
            passwordError.setVisibility(View.GONE);
            confirmPasswordError.setVisibility(View.GONE);
            errorText.setVisibility(View.GONE);
            etEmail.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.validated_fields));
            etPassword.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.validated_fields));
            etConfirmPassword.setBackground(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.validated_fields));
         //   radioTerms.setButtonDrawable(ContextCompat.getDrawable(EmailSignupActivity.this, R.drawable.checked));
            Intent intent = new Intent(EmailSignupActivity.this, VerificationCodeActivity.class);
            startActivity(intent);
        }
    }
    private void initViews() {
        radioTerms = findViewById(R.id.radioTerms);
        signUp = findViewById(R.id.signUp);
        btnNext = findViewById(R.id.btnNext);
        checkboxContainer = findViewById(R.id.checkboxContainer);
        close = findViewById(R.id.close);
        emailError = findViewById(R.id.emailError);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        passwordError = findViewById(R.id.passwordError);
        confirmPasswordError = findViewById(R.id.confirmPasswordError);
        errorText = findViewById(R.id.errorText);
        login = "Login";
        String spanTextLogin = getText(R.string.already_have_an_acc) + login;
        SpannableStringBuilder loginBuilder = new SpannableStringBuilder(spanTextLogin);
        int flg = 0;
        loginBuilder.setSpan(new TextAppearanceSpan(this, R.style.SpanTextAppearance1), 0, spanTextLogin.length(), flg);
        signUp.setText((Html.fromHtml("<font color=\"#ffffff\">" + getText(R.string.already_have_an_acc) + "</font>" + "<font color=\"#0D9B6B\">" + " " + login + "</font>")));
        privacyText = findViewById(R.id.privacyText);
        String spanText = getText(R.string.by_signing_up_you_agree_to_weedgifter) + " privacy policy" + " and " + " terms and conditions.";
        SpannableString ssBuilder = new SpannableString(spanText);
        int flag = 0;
        ssBuilder.setSpan(new TextAppearanceSpan(this, R.style.SpanTextAppearance1), 0, spanText.length(), flag);
        privacyText.setText((Html.fromHtml("<font color=\"#ffffff\">" + getText(R.string.by_signing_up_you_agree_to_weedgifter) + "</font>" + "<font color=\"#0D9B6B\">" + " privacy policy" + "</font>" + "<font color=\"#ffffff\">" + " and " + "</font>" + "<font color=\"#0D9B6B\">" + " terms and conditions.")));
    }
}
