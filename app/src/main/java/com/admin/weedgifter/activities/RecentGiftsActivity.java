package com.admin.weedgifter.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.admin.weedgifter.R;
import com.admin.weedgifter.fragments.ClaimedFragment;
import com.admin.weedgifter.fragments.FavouritesFragment;
import com.admin.weedgifter.fragments.ReceivedFragment;

public class RecentGiftsActivity extends AppCompatActivity {
    private TextView tvReceived, tvClaimed, tvFavourites;
    private FrameLayout receivedFrame,claimedFrame,favouritesFrame;
    private LinearLayout receivedContainer,claimedContainer,favouritesContainer;
    private ImageView close;
    private View receivedSelected,receivedUnselected,claimedSelected,claimedUnselected,favouritesSelected,favouritesUnselected;
    private ReceivedFragment receivedFragment = new ReceivedFragment();
    private ClaimedFragment claimedFragment = new ClaimedFragment();
    private FavouritesFragment favouritesFragment = new FavouritesFragment();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_gifts);
        initViews();
        clickListners();
    }
    private void clickListners() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        receivedContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                receivedFrame.setVisibility(View.VISIBLE);
                claimedFrame.setVisibility(View.GONE);
                favouritesFrame.setVisibility(View.GONE);
                tvReceived.setTextColor(Color.parseColor("#0D9B6B"));
                tvClaimed.setTextColor(Color.parseColor("#A7A7A7"));
                tvFavourites.setTextColor(Color.parseColor("#A7A7A7"));
                receivedSelected.setVisibility(View.VISIBLE);
                receivedUnselected.setVisibility(View.GONE);
                claimedSelected.setVisibility(View.GONE);
                claimedUnselected.setVisibility(View.VISIBLE);
                favouritesSelected.setVisibility(View.GONE);
                favouritesUnselected.setVisibility(View.VISIBLE);
                FragmentTransaction fragementTransaction = getSupportFragmentManager().beginTransaction();
                fragementTransaction.replace(R.id.receivedFrame, receivedFragment);
                fragementTransaction.commit();

            }
        });
        claimedContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                claimedFrame.setVisibility(View.VISIBLE);
                receivedFrame.setVisibility(View.GONE);
                favouritesFrame.setVisibility(View.GONE);
                tvClaimed.setTextColor(Color.parseColor("#0D9B6B"));
                tvReceived.setTextColor(Color.parseColor("#A7A7A7"));
                tvFavourites.setTextColor(Color.parseColor("#A7A7A7"));
                receivedSelected.setVisibility(View.GONE);
                receivedUnselected.setVisibility(View.VISIBLE);
                claimedSelected.setVisibility(View.VISIBLE);
                claimedUnselected.setVisibility(View.GONE);
                favouritesSelected.setVisibility(View.GONE);
                favouritesUnselected.setVisibility(View.VISIBLE);
                FragmentTransaction fragementTransaction = getSupportFragmentManager().beginTransaction();
                fragementTransaction.replace(R.id.claimedFrame, claimedFragment);
                fragementTransaction.commit();

            }
        });
        favouritesContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                favouritesFrame.setVisibility(View.VISIBLE);
                claimedFrame.setVisibility(View.GONE);
                receivedFrame.setVisibility(View.GONE);
                tvReceived.setTextColor(Color.parseColor("#A7A7A7"));
                tvClaimed.setTextColor(Color.parseColor("#A7A7A7"));
                tvFavourites.setTextColor(Color.parseColor("#0D9B6B"));
                receivedSelected.setVisibility(View.GONE);
                receivedUnselected.setVisibility(View.VISIBLE);
                claimedSelected.setVisibility(View.GONE);
                claimedUnselected.setVisibility(View.VISIBLE);
                favouritesSelected.setVisibility(View.VISIBLE);
                favouritesUnselected.setVisibility(View.GONE);
                FragmentTransaction fragementTransaction = getSupportFragmentManager().beginTransaction();
                fragementTransaction.replace(R.id.favouritesFrame, favouritesFragment);
                fragementTransaction.commit();
            }
        });
    }

    private void initViews() {
        close = findViewById(R.id.close);
        tvReceived = findViewById(R.id.tvReceived);
        tvClaimed = findViewById(R.id.tvClaimed);
        tvFavourites = findViewById(R.id.tvFavourites);
        receivedFrame = findViewById(R.id.receivedFrame);
        claimedFrame = findViewById(R.id.claimedFrame);
        favouritesFrame = findViewById(R.id.favouritesFrame);
        receivedSelected = findViewById(R.id.receivedSelected);
        receivedUnselected = findViewById(R.id.receivedUnselected);
        claimedSelected = findViewById(R.id.claimedSelected);
        claimedUnselected = findViewById(R.id.claimedUnselected);
        favouritesSelected = findViewById(R.id.favouritesSelected);
        favouritesUnselected = findViewById(R.id.favouritesUnselected);
        receivedContainer = findViewById(R.id.receivedContainer);
        claimedContainer = findViewById(R.id.claimedContainer);
        favouritesContainer = findViewById(R.id.favouritesContainer);
        tvReceived.setTextColor(Color.parseColor("#0D9B6B"));
        tvClaimed.setTextColor(Color.parseColor("#A7A7A7"));
        tvFavourites.setTextColor(Color.parseColor("#A7A7A7"));
        receivedFrame.setVisibility(View.VISIBLE);
        receivedSelected.setVisibility(View.VISIBLE);
        receivedUnselected.setVisibility(View.GONE);
        claimedSelected.setVisibility(View.GONE);
        claimedUnselected.setVisibility(View.VISIBLE);
        favouritesSelected.setVisibility(View.GONE);
        favouritesUnselected.setVisibility(View.VISIBLE);
        claimedFrame.setVisibility(View.GONE);
        favouritesFrame.setVisibility(View.GONE);
        FragmentTransaction fragementTransaction = getSupportFragmentManager().beginTransaction();
        fragementTransaction.replace(R.id.receivedFrame, receivedFragment);
        fragementTransaction.commit();
    }
}
