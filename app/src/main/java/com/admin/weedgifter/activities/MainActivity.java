package com.admin.weedgifter.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.admin.weedgifter.R;
import com.admin.weedgifter.globalConstants.Constants;
import com.admin.weedgifter.sharedpreferences.SharedPrefHandler;
import com.admin.weedgifter.utils.Common;
import com.admin.weedgifter.utils.NetworkState;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "Facebook token";
    private LoginButton fbLoginBtn;
    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    private TextView signUp, asGuest;
    Button fbCustom, gmailCustom, emailSign;
    SignInButton signInButton;
    private String firstName, lastName, email, id, name, imgUrl, idToken;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private static final int RC_SIGN_IN = 1;
    private GoogleApiClient googleApiClient;
    private LinearLayout emailContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        fbLogin();
        googleLogin();
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SignupActivity.class);
                startActivity(intent);
                finish();
            }
        });
        emailSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, EmailLoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        asGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                //  intent.putExtra("loginType","guest");
                startActivity(intent);
                finish();
            }
        });
    }
    public void onClickFacebookButton(View view) {
        if (view == fbCustom) {
            fbLoginBtn.performClick();
        }
    }
    public void onClickGmailbutton(View view) {
        if (view == gmailCustom) {
            Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
            startActivityForResult(intent, RC_SIGN_IN);
        }
    }
    private void googleLogin() {
        firebaseAuth = com.google.firebase.auth.FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                //if settings is signed in, we call a helper method to save the settings details to Firebase
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.web_client_id))
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        signInButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent, RC_SIGN_IN);
            }
        });
    }
    private void fbLogin() {
        boolean loggedOut = AccessToken.getCurrentAccessToken() == null;
        fbLoginBtn.setReadPermissions(Arrays.asList("email", "public_profile"));
        callbackManager = CallbackManager.Factory.create();
        fbLoginBtn.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (AccessToken.getCurrentAccessToken() != null) {
                    handleFacebookAccessToken(loginResult.getAccessToken());
                } else {
                    Common.displayToast(MainActivity.this, "Access token is null");
                }
            }
            @Override
            public void onCancel() {
                Log.e("fb", "close");
                AccessToken fb_token = AccessToken.getCurrentAccessToken();
                if (fb_token != null) {
                    LoginManager.getInstance().logOut();
                }
            }
            @Override
            public void onError(FacebookException exception) {
            }
        });
    }
    private void initViews() {
        signInButton = findViewById(R.id.sign_in_button);
        fbLoginBtn = findViewById(R.id.fbLoginBtn);
        asGuest = findViewById(R.id.asGuest);
        fbCustom = findViewById(R.id.fbcustom);
        gmailCustom = findViewById(R.id.gmailCustom);
        emailSign = findViewById(R.id.emailSign);
        signUp = findViewById(R.id.signUp);
        String spanText = getText(R.string.new_to_weedmaps_sign_up) + "REGISTER";
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(spanText);
        int flag = 0;
        ssBuilder.setSpan(new TextAppearanceSpan(this, R.style.SpanTextAppearance1), 0, spanText.length(), flag);
        signUp.setText((Html.fromHtml("<font color=\"#ffffff\">" + getText(R.string.new_to_weedmaps_sign_up) + "</font>" + "<font color=\"#0D9B6B\">" + " REGISTER" + "</font>")));
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }
    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            assert account != null;
            idToken = account.getIdToken();
            name = account.getDisplayName();
            email = account.getEmail();
            // you can store settings data to SharedPreference
            AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
            firebaseAuthWithGoogle(credential);
        } else {
            // Google Sign In failed, update UI appropriately
            Log.e(TAG, "Login Unsuccessful. " + result);
            Common.displayToast(this, "Login Unsuccessful");
        }
    }
    private void firebaseAuthWithGoogle(AuthCredential credential) {
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        if (task.isSuccessful()) {
                            Common.displayToast(MainActivity.this, getString(R.string.login_success));
                            gotoProfile();
                        } else {
                            Log.w(TAG, "signInWithCredential" + task.getException().getMessage());
                            task.getException().printStackTrace();
                            Common.displayToast(MainActivity.this, getString(R.string.auth_failed));
                        }
                    }
                });
    }
    private void gotoProfile() {
        SharedPrefHandler.setBooleanValues(MainActivity.this, Constants.ISLOGIN, true);
        Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        GraphRequest request = GraphRequest.newMeRequest(
                token, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("TAG", object.toString());
                        try {
                            firstName = object.getString("first_name");
                            lastName = object.getString("last_name");
                            email = object.getString("email");
                            id = object.getString("id");
                            imgUrl = "https://graph.facebook.com/" + id + "/picture?type=normal";
                            Log.e("First Name: ", firstName + "\nLast Name: " + lastName);
                            gotoProfile();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AccessToken fb_token = AccessToken.getCurrentAccessToken();
        if (fb_token != null) {
            LoginManager.getInstance().logOut();
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        if (authStateListener != null) {
            FirebaseAuth.getInstance().signOut();
        }
        assert authStateListener != null;
        firebaseAuth.addAuthStateListener(authStateListener);
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (authStateListener != null) {
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}
}


