package com.admin.weedgifter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.admin.weedgifter.R;

public class PasswordResetSuccessfullyActivity extends AppCompatActivity {
    private Button btnBackToLogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset_successfully);
        btnBackToLogin = findViewById(R.id.btnBackToLogin);
        btnBackToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PasswordResetSuccessfullyActivity.this, EmailLoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
