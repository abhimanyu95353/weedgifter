package com.admin.weedgifter.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.admin.weedgifter.R;
import com.admin.weedgifter.fragments.ProductsReviewFragment;
import com.admin.weedgifter.fragments.StoreReviewFragment;

public class MyReviewsActivity extends AppCompatActivity {
    private View productSelected, productUnselected, storeSelected, storeUnselected;
    private ImageView close;
    private ProductsReviewFragment productsReviewFragment = new ProductsReviewFragment();
    private StoreReviewFragment storeReviewFragment = new StoreReviewFragment();
    private FrameLayout storeReviewFrag, productsReviewFrag;
    private TextView storesReview, productsReview;
    private LinearLayout storesContainer, productsContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_reviews);
        initViews();
        clickListners();
    }
    private void clickListners() {
        storesContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productsReviewFrag.setVisibility(View.GONE);
                storeReviewFrag.setVisibility(View.VISIBLE);
                storeSelected.setVisibility(View.VISIBLE);
                storeUnselected.setVisibility(View.GONE);
                productSelected.setVisibility(View.GONE);
                productUnselected.setVisibility(View.VISIBLE);
                storesReview.setTextColor(Color.parseColor("#0D9B6B"));
                productsReview.setTextColor(Color.parseColor("#A7A7A7"));
                FragmentTransaction fragementTransaction = getSupportFragmentManager().beginTransaction();
                fragementTransaction.replace(R.id.storeReviewFrag, storeReviewFragment);
                fragementTransaction.commit();
            }
        });
        productsContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productsReviewFrag.setVisibility(View.VISIBLE);
                storeReviewFrag.setVisibility(View.GONE);
                storeSelected.setVisibility(View.GONE);
                storeUnselected.setVisibility(View.VISIBLE);
                productSelected.setVisibility(View.VISIBLE);
                productUnselected.setVisibility(View.GONE);
                productsReview.setTextColor(Color.parseColor("#0D9B6B"));
                storesReview.setTextColor(Color.parseColor("#A7A7A7"));
                FragmentTransaction fragementTransaction = getSupportFragmentManager().beginTransaction();
                fragementTransaction.replace(R.id.productsReviewFrag, productsReviewFragment);
                fragementTransaction.commit();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void initViews() {
        productsReview = findViewById(R.id.productReview);
        storesReview = findViewById(R.id.storeReview);
        storeReviewFrag = findViewById(R.id.storeReviewFrag);
        productsReviewFrag = findViewById(R.id.productsReviewFrag);
        close = findViewById(R.id.close);
        storesContainer = findViewById(R.id.storesContainer);
        productsContainer = findViewById(R.id.productsContainer);
        storeSelected = findViewById(R.id.storeSelected);
        storeUnselected = findViewById(R.id.storeUnselected);
        productSelected = findViewById(R.id.productSelected);
        productUnselected = findViewById(R.id.productUnselected);
        FragmentTransaction fragementTransaction = getSupportFragmentManager().beginTransaction();
        fragementTransaction.replace(R.id.storeReviewFrag, storeReviewFragment);
        fragementTransaction.commit();
        storeReviewFrag.setVisibility(View.VISIBLE);
        productsReviewFrag.setVisibility(View.GONE);
        storeSelected.setVisibility(View.VISIBLE);
        storeUnselected.setVisibility(View.GONE);
        productSelected.setVisibility(View.GONE);
        productUnselected.setVisibility(View.VISIBLE);
        storesReview.setTextColor(Color.parseColor("#0D9B6B"));
        productsReview.setTextColor(Color.parseColor("#A7A7A7"));
    }
}
