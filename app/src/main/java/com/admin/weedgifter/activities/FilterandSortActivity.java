package com.admin.weedgifter.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import com.admin.weedgifter.R;
public class FilterandSortActivity extends AppCompatActivity {
    private ImageView close, filterImg, sortImg;
    private LinearLayout sortLayout, filterLayout, filterView, sortView;
    private RelativeLayout layoutLicenseType,layoutCategories,layoutAmenities;
    private RadioButton radioFeatured, radioMostReviewed, radioHighestRated, radioAz, radioZa;
    private TextView tvFeatured,tvHighest,tvMostReviewed,tvAz,tvZa;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_and_sort);
        initViews();
        clickListners();
    }
    private void clickListners() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        filterLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterLayout.setBackground(ContextCompat.getDrawable(FilterandSortActivity.this, R.drawable.selected_store_filter_bg));
                sortLayout.setBackground(ContextCompat.getDrawable(FilterandSortActivity.this, R.drawable.unselected_item_filter_bg));
                filterImg.setColorFilter(ContextCompat.getColor(FilterandSortActivity.this, R.color.white), PorterDuff.Mode.SRC_IN);
                sortImg.setColorFilter(ContextCompat.getColor(FilterandSortActivity.this, R.color.colorTertiary), PorterDuff.Mode.SRC_IN);
                filterView.setVisibility(View.VISIBLE);
                sortView.setVisibility(View.GONE);

            }
        });
        sortLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sortLayout.setBackground(ContextCompat.getDrawable(FilterandSortActivity.this, R.drawable.selected_item_filter_bg));
                filterLayout.setBackground(ContextCompat.getDrawable(FilterandSortActivity.this, R.drawable.unselected_store_filter_bg));
                sortImg.setColorFilter(ContextCompat.getColor(FilterandSortActivity.this, R.color.white), PorterDuff.Mode.SRC_IN);
                filterImg.setColorFilter(ContextCompat.getColor(FilterandSortActivity.this, R.color.colorTertiary), PorterDuff.Mode.SRC_IN);
                filterView.setVisibility(View.GONE);
                sortView.setVisibility(View.VISIBLE);
            }
        });
        radioFeatured.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (radioFeatured.isChecked()) {
                    tvFeatured.setTextColor(Color.parseColor("#22B573"));
                    tvMostReviewed.setTextColor(Color.WHITE);
                    tvHighest.setTextColor(Color.WHITE);
                    tvZa.setTextColor(Color.WHITE);
                    tvAz.setTextColor(Color.WHITE);
                    radioAz.setChecked(false);
                    radioZa.setChecked(false);
                    radioMostReviewed.setChecked(false);
                    radioHighestRated.setChecked(false);
                }
            }
        });
        radioHighestRated.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (radioHighestRated.isChecked()) {
                    tvHighest.setTextColor(Color.parseColor("#22B573"));
                    tvMostReviewed.setTextColor(Color.WHITE);
                    tvFeatured.setTextColor(Color.WHITE);
                    tvZa.setTextColor(Color.WHITE);
                    tvAz.setTextColor(Color.WHITE);
                    radioFeatured.setChecked(false);
                    radioAz.setChecked(false);
                    radioZa.setChecked(false);
                    radioMostReviewed.setChecked(false);
                }
            }
        });
        radioMostReviewed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (radioMostReviewed.isChecked()) {
                    tvMostReviewed.setTextColor(Color.parseColor("#22B573"));
                    tvFeatured.setTextColor(Color.WHITE);
                    tvHighest.setTextColor(Color.WHITE);
                    tvZa.setTextColor(Color.WHITE);
                    tvAz.setTextColor(Color.WHITE);
                    radioFeatured.setChecked(false);
                    radioAz.setChecked(false);
                    radioZa.setChecked(false);
                    radioHighestRated.setChecked(false);
                }
            }
        });
        radioAz.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (radioAz.isChecked()) {
                    tvAz.setTextColor(Color.parseColor("#22B573"));
                    tvMostReviewed.setTextColor(Color.WHITE);
                    tvHighest.setTextColor(Color.WHITE);
                    tvZa.setTextColor(Color.WHITE);
                    tvFeatured.setTextColor(Color.WHITE);
                    radioFeatured.setChecked(false);
                    radioZa.setChecked(false);
                    radioHighestRated.setChecked(false);
                    radioMostReviewed.setChecked(false);
                }
            }
        });
        radioZa.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (radioZa.isChecked()) {
                    tvZa.setTextColor(Color.parseColor("#22B573"));
                    tvMostReviewed.setTextColor(Color.WHITE);
                    tvHighest.setTextColor(Color.WHITE);
                    tvFeatured.setTextColor(Color.WHITE);
                    tvAz.setTextColor(Color.WHITE);
                    radioFeatured.setChecked(false);
                    radioAz.setChecked(false);
                    radioHighestRated.setChecked(false);
                    radioMostReviewed.setChecked(false);
                }
            }
        });

        layoutLicenseType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FilterandSortActivity.this, LicenseTypeFilterActivity.class);
                startActivity(intent);
            }
        });
        layoutCategories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FilterandSortActivity.this, CategoryTypeFilterActivity.class);
                startActivity(intent);
            }
        });
        layoutAmenities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FilterandSortActivity.this,AmenitiesTypeFilterActivity.class);
                startActivity(intent);
            }
        });
    }
    private void initViews() {
        close = findViewById(R.id.close);
        sortLayout = findViewById(R.id.sortLayout);
        filterLayout = findViewById(R.id.filterLayout);
        filterImg = findViewById(R.id.filterImg);
        sortImg = findViewById(R.id.sortImg);
        filterView = findViewById(R.id.filterView);
        sortView = findViewById(R.id.sortView);
        radioFeatured = findViewById(R.id.radioFeatured);
        radioAz = findViewById(R.id.radioAz);
        radioZa = findViewById(R.id.radioZa);
        radioHighestRated = findViewById(R.id.radioHighestRated);
        radioMostReviewed = findViewById(R.id.radioMostReviewed);
        tvFeatured = findViewById(R.id.tvFeatured);
        tvHighest = findViewById(R.id.tvHighest);
        tvMostReviewed = findViewById(R.id.tvMostReviewed);
        tvAz = findViewById(R.id.tvAz);
        tvZa = findViewById(R.id.tvZa);
        layoutAmenities = findViewById(R.id.layoutAmenities);
        layoutCategories = findViewById(R.id.layoutCategories);
        layoutLicenseType = findViewById(R.id.layoutLicenseType);
        filterView.setVisibility(View.VISIBLE);
        sortView.setVisibility(View.GONE);
    }
}
