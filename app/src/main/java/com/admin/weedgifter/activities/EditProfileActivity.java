package com.admin.weedgifter.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.admin.weedgifter.R;
import com.admin.weedgifter.utils.Common;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class EditProfileActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {
    private EditText etAddress, etDob, etName, etCity, etState, etAbout;
    private String name, gender, dob, address, city, state, about;
    private ImageView close, spinnerArrow, uploadPic, uploadedPic;
    private TextView errorText;
    final Calendar myCalendar = Calendar.getInstance();
    private Spinner etGender;
    private boolean spinnerInit = false;
    private Button btnNext;
    DatePickerDialog.OnDateSetListener datePickerDialog;
    private AlertDialog alertDialog;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int RESULT_LOAD_IMAGE = 10;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initViews();
        clickListners();
    }
    private void initViews() {
        close = findViewById(R.id.close);
        etAddress = findViewById(R.id.etAddress);
        etDob = findViewById(R.id.etDob);
        etName = findViewById(R.id.etName);
        errorText = findViewById(R.id.errorText);
        etCity = findViewById(R.id.etCity);
        etState = findViewById(R.id.etState);
        etGender = findViewById(R.id.etGender);
        spinnerArrow = findViewById(R.id.spinnerArrow);
        btnNext = findViewById(R.id.btnNext);
        etAbout = findViewById(R.id.etAbout);
        uploadPic = findViewById(R.id.uploadPic);
        uploadedPic = findViewById(R.id.uploadedPic);
        datePickerDialog = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        uploadPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImageDialog();
            }
        });
    }

    private void clickListners() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNextClick();
            }
        });
        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add(getString(R.string.male));
        categories.add(getString(R.string.female));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_gender_item, categories);
        dataAdapter.setDropDownViewResource(R.layout.spinner_gender_item);
        etGender.setAdapter(dataAdapter);
        etGender.setOnItemSelectedListener(this);
        etDob.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    Common.hideKeyboard(EditProfileActivity.this, view);
                    new DatePickerDialog(EditProfileActivity.this, datePickerDialog, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                } else {
                    dob = etDob.getText().toString().trim();
                    if (dob.equals("")) {
                        return;
                    } else {
                        Common.hideKeyboard(EditProfileActivity.this, view);
                        errorText.setVisibility(View.GONE);
                    }
                }
            }
        });
        etDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(EditProfileActivity.this, datePickerDialog, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }
    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        etDob.setText(sdf.format(myCalendar.getTime()));
    }
    private void onNextClick() {
        name = etName.getText().toString();
        //    String gender = etGender.getText().toString();
        dob = etDob.getText().toString();
        address = etAddress.getText().toString();
        city = etCity.getText().toString();
        state = etState.getText().toString();
        about = etAbout.getText().toString();
        if (name.equals("") && address.equals("") && dob.equals("") && address.equals("") && city.equals("") && address.equals("")) {
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
            etName.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.error_bg));
            etAddress.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.error_bg));
            etDob.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.error_bg));
            etCity.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.error_bg));
            etState.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.error_bg));
            etAbout.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.error_bg));
        } else if (name.equals("")) {
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
            etName.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.error_bg));
        } else if (dob.equals("")) {
            etDob.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.error_bg));
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
        } else if (address.equals("")) {
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
            etAddress.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.error_bg));
        } else if (city.equals("")) {
            etCity.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.error_bg));
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
        } else if (state.equals("")) {
            etState.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.error_bg));
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
        } else if (about.equals("")) {
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
            etAbout.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.error_bg));
        } else {
            errorText.setVisibility(View.GONE);
            etName.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.edit_text_unfocused_bg));
            etAddress.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.edit_text_unfocused_bg));
            etDob.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.edit_text_unfocused_bg));
            etCity.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.edit_text_unfocused_bg));
            etState.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.edit_text_unfocused_bg));
            etAbout.setBackground(ContextCompat.getDrawable(EditProfileActivity.this, R.drawable.edit_text_unfocused_bg));
            finish();
        }
    }
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {}
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {}
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            if (data != null) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                uploadedPic.setImageBitmap(imageBitmap);
                alertDialog.dismiss();
            }
        } else if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK) {
            if (data != null) {
                Uri imageUri = data.getData();
                uploadedPic.setImageURI(imageUri);
                alertDialog.dismiss();
                Common.displayToast(this,"Photo Mapped Success.");
            }
        }
    }
    private void uploadImageDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mView = inflater.inflate(R.layout.dialog_upload_video_image, null);
        alert.setView(mView);
        alertDialog = alert.create();
        alertDialog.setCanceledOnTouchOutside(false);
        TextView tvGallery = mView.findViewById(R.id.tvGallery);
        TextView tvCamera = mView.findViewById(R.id.tvCamera);
        TextView titleText = mView.findViewById(R.id.titleText);
        TextView textInfo = mView.findViewById(R.id.textInfo);
        titleText.setText(getText(R.string.upload_image));
        textInfo.setText(getText(R.string.take_a_new_photo_or_select_one_from_your_photo_gallery));
        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });
        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }
}
