package com.admin.weedgifter.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.CommentsListAdapter;

public class ReviewActiivty extends AppCompatActivity {
    private RecyclerView commentList;
    private EditText etReview;
    private TextView savePost;
    private ImageView close;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        initViews();
        clickListner();
        setCommentList();
    }
    private void clickListner() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        savePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        etReview.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etReview.setBackground(ContextCompat.getDrawable(ReviewActiivty.this, R.drawable.edit_text_in_focus_bg));
                    if (!etReview.getText().toString().isEmpty()) {
                        savePost.setTextColor(Color.parseColor("#0D9B6B"));
                    }
                }
            }
        });
    }
    private void setCommentList() {
        CommentsListAdapter commentsListAdapter = new CommentsListAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        commentList.setAdapter(commentsListAdapter);
        commentList.setLayoutManager(linearLayoutManager);
        commentsListAdapter.notifyDataSetChanged();
    }
    private void initViews() {
        commentList = findViewById(R.id.commentList);
        savePost = findViewById(R.id.savePost);
        etReview = findViewById(R.id.etReview);
        close = findViewById(R.id.close);
    }
}
