package com.admin.weedgifter.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.ThemesListAdapter;
import com.admin.weedgifter.interfaces.ThemeSelector;

public class SendGiftActivity extends AppCompatActivity implements ThemeSelector {
    private RecyclerView themesList;
    private View line;
    private RelativeLayout giftLinkLayout;
    private ImageView viewGift, close,bg;
    private boolean isRotated = false;
    private RelativeLayout parentLayout;
    private TextView bankName;
    private Button payBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_gift);
        initViews();
        setThemesView();
        clickListners();
    }
    private void clickListners() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        viewGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                line.setVisibility(View.GONE);
                giftLinkLayout.setVisibility(View.GONE);
                if (!isRotated) {
                    viewGift.animate().rotation(180).start();
                    isRotated = true;
                } else {
                    viewGift.animate().rotation(0).start();
                    isRotated = false;
                    line.setVisibility(View.VISIBLE);
                    giftLinkLayout.setVisibility(View.VISIBLE);
                }
            }
        });
        bankName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SendGiftActivity.this,SelectBankActivity.class);
                startActivity(intent);
            }
        });
        payBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SendGiftActivity.this,EnterPinActivity.class);
                startActivity(intent);
            }
        });
    }
    private void setThemesView() {
        ThemesListAdapter themesListAdapter = new ThemesListAdapter(this, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        themesList.setAdapter(themesListAdapter);
        themesList.setLayoutManager(linearLayoutManager);
        themesListAdapter.notifyDataSetChanged();
    }
    private void initViews() {
        themesList = findViewById(R.id.themesList);
        line = findViewById(R.id.line);
        giftLinkLayout = findViewById(R.id.giftLinkLayout);
        viewGift = findViewById(R.id.viewGift);
        close = findViewById(R.id.close);
        parentLayout = findViewById(R.id.parentLayout);
        bankName = findViewById(R.id.bankName);
        payBtn =findViewById(R.id.payBtn);
        bg = findViewById(R.id.bg);
    }
    @Override
    public void onThemeSelected(String themeName) {
        switch (themeName) {
            case "churchTheme":
                parentLayout.setBackgroundColor(Color.parseColor("#0D9B6B"));
                bg.setBackground(ContextCompat.getDrawable(this, R.drawable.send_gift_bg));
                break;
            case "partyTheme":
            case "globeTheme":
            case "yellowishTheme":
                parentLayout.setBackgroundColor(Color.parseColor("#0A4A75"));
                bg.setBackground(ContextCompat.getDrawable(this, R.drawable.party_theme_bg));
                break;
        }
    }
}
