package com.admin.weedgifter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.admin.weedgifter.R;
import com.admin.weedgifter.utils.Common;

public class ForgotPasswordActivity extends AppCompatActivity {
    private EditText etEmail;
    private String email;
    private TextView errorText;
    private Button btnSendRequest;
    private ImageView close;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initViews();
        clickListners();
    }
    private void initViews() {
        etEmail = findViewById(R.id.etEmail);
        errorText = findViewById(R.id.errorText);
        btnSendRequest = findViewById(R.id.btnSendRequest);
        close = findViewById(R.id.close);
        etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etEmail.setBackground(ContextCompat.getDrawable(ForgotPasswordActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    email = etEmail.getText().toString().trim();
                    if (email.equals("")) {
                        return;
                    } else if (!Common.isEmailValid(email)) {
                        errorText.setVisibility(View.VISIBLE);
                        etEmail.setBackground(ContextCompat.getDrawable(ForgotPasswordActivity.this, R.drawable.error_bg));
                        errorText.setText(getText(R.string.enter_app_email));
                    } else {
                        Common.hideKeyboard(ForgotPasswordActivity.this, etEmail);
                        etEmail.setBackground(ContextCompat.getDrawable(ForgotPasswordActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                    }
                }
            }
        });
        etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || event != null &&
                        event.getAction() == KeyEvent.ACTION_DOWN &&
                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    if (event == null || !event.isShiftPressed()) {
                        // the user is done typing.
                        email = etEmail.getText().toString();
                        if (email.equals("")) {
                            etEmail.setBackground(ContextCompat.getDrawable(ForgotPasswordActivity.this, R.drawable.error_bg));
                            errorText.setVisibility(View.VISIBLE);
                            errorText.setText(getText(R.string.fill_all_fields));
                        } else if (!Common.isEmailValid(email)) {
                            errorText.setVisibility(View.VISIBLE);
                            etEmail.setBackground(ContextCompat.getDrawable(ForgotPasswordActivity.this, R.drawable.error_bg));
                            errorText.setText(getText(R.string.enter_app_email));
                        } else {
                            etEmail.setBackground(ContextCompat.getDrawable(ForgotPasswordActivity.this, R.drawable.validated_fields));
                            btnSendRequest.setBackground(ContextCompat.getDrawable(ForgotPasswordActivity.this, R.drawable.primary_color_btn_bg_filled));
                            errorText.setVisibility(View.GONE);
                            Common.hideKeyboard(ForgotPasswordActivity.this, btnSendRequest);
                            Intent intent = new Intent(ForgotPasswordActivity.this, EmailVerificationCodeActivity.class);
                            startActivity(intent);
                            Common.displayToast(ForgotPasswordActivity.this,getString(R.string.request_sent));
                        }
                        return false;
                    }
                }
                return false;
            }
        });
    }
    private void clickListners() {
        btnSendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSendRequestButtonClick();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void onSendRequestButtonClick() {
        email = etEmail.getText().toString().trim();
        if (email.equals("")) {
            etEmail.setBackground(ContextCompat.getDrawable(ForgotPasswordActivity.this, R.drawable.error_bg));
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
        } else if (!(Common.isEmailValid(email))) {
            errorText.setVisibility(View.VISIBLE);
            etEmail.setBackground(ContextCompat.getDrawable(ForgotPasswordActivity.this, R.drawable.error_bg));
            errorText.setText(getText(R.string.enter_app_email));
        } else {
            etEmail.setBackground(ContextCompat.getDrawable(ForgotPasswordActivity.this, R.drawable.validated_fields));
            errorText.setVisibility(View.GONE);
            Common.hideKeyboard(this, btnSendRequest);
            Intent intent = new Intent(ForgotPasswordActivity.this, EmailVerificationCodeActivity.class);
            startActivity(intent);
            Common.displayToast(ForgotPasswordActivity.this,getString(R.string.request_sent));
        }
    }
}
