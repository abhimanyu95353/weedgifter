package com.admin.weedgifter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.admin.weedgifter.R;
import com.admin.weedgifter.utils.Common;

public class AddPaymentMethodActivity extends AppCompatActivity {
    private TextView signIn, errorText;
    private Button btnNext;
    private ImageView close;
    private String cardNumber, expiration, cvv, cardHolderName;
    private EditText etCardNumber, etExpiration, etCvv, etCardHolderName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment_method);
        initViews();
        clickListners();
    }
    private void clickListners() {
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddPaymentMethodActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardNumber = etCardNumber.getText().toString().trim();
                expiration = etExpiration.getText().toString().trim();
                cvv = etCvv.getText().toString().trim();
                cardHolderName = etCardHolderName.getText().toString().trim();
                if (cardNumber.equals("") && expiration.equals("") && cvv.equals("") && cardHolderName.equals("")) {
                    etCardNumber.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.error_bg));
                    etExpiration.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.error_bg));
                    etCvv.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.error_bg));
                    etCardHolderName.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.error_bg));
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(getText(R.string.fill_all_fields));
                } else if (cardNumber.equals("")) {
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(getText(R.string.fill_all_fields));
                    etCardNumber.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.error_bg));
                } else if (expiration.equals("")) {
                    etExpiration.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.error_bg));
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(getText(R.string.fill_all_fields));
                } else if (cvv.equals("")) {
                    etCvv.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.error_bg));
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(getText(R.string.fill_all_fields));
                } else if (cardHolderName.equals("")) {
                    etCardHolderName.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.error_bg));
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(getText(R.string.fill_all_fields));
                } else if (!Common.isCardNumberValid(cardNumber)) {
                    etCardNumber.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.error_bg));
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(getText(R.string.card_number_limit));
                } else if (!Common.isCvvValid(cvv)) {
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(getText(R.string.enter_valid_cvv));
                    etCvv.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.error_bg));
                } else {
                    Intent intent = new Intent(AddPaymentMethodActivity.this, GeneratePinCodeActivity.class);
                    startActivity(intent);
                }
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        etCardNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etCardNumber.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    cardNumber = etCardNumber.getText().toString().trim();
                    if (cardNumber.isEmpty()) {
                        return;
                    } else if (!Common.isCardNumberValid(cardNumber)) {
                        etCardNumber.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.error_bg));
                        errorText.setVisibility(View.VISIBLE);
                        errorText.setText(getText(R.string.card_number_limit));
                    } else {
                        etCardNumber.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                    }
                }
            }
        });
        etExpiration.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etExpiration.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    expiration = etExpiration.getText().toString().trim();
                    if (expiration.isEmpty()) {
                        return;
                    } else {
                        etExpiration.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.validated_fields));
                    }
                }
            }
        });
        etCvv.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etCvv.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    cvv = etCvv.getText().toString().trim();
                    if (cvv.isEmpty()) {
                        return;
                    } else if (!Common.isCvvValid(cvv)) {
                        errorText.setVisibility(View.VISIBLE);
                        errorText.setText(getText(R.string.enter_valid_cvv));
                        etCvv.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.error_bg));
                    } else {
                        errorText.setVisibility(View.GONE);
                        etCvv.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.validated_fields));
                    }
                }
            }
        });
        etCardHolderName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etCardHolderName.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    cardHolderName = etCardHolderName.getText().toString().trim();
                    if (cardHolderName.equals("")) {
                        return;
                    } else {
                        etCardHolderName.setBackground(ContextCompat.getDrawable(AddPaymentMethodActivity.this, R.drawable.validated_fields));
                        Common.hideKeyboard(AddPaymentMethodActivity.this, view);
                    }
                }
            }
        });
    }

    private void initViews() {
        etCardNumber = findViewById(R.id.etCardNumber);
        etExpiration = findViewById(R.id.etExpiration);
        etCvv = findViewById(R.id.etCvv);
        errorText = findViewById(R.id.errorText);
        etCardHolderName = findViewById(R.id.etCardHolderName);
        close = findViewById(R.id.close);
        btnNext = findViewById(R.id.btnNext);
        signIn = findViewById(R.id.signIn);
        String spanText = getText(R.string.dont_have_an_acc) + "LOGIN";
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(spanText);
        int flag = 0;
        ssBuilder.setSpan(new TextAppearanceSpan(this, R.style.SpanTextAppearance1), 0, spanText.length(), flag);
        signIn.setText((Html.fromHtml("<font color=\"#ffffff\">" + getText(R.string.new_to_weedmaps_sign_up) + "</font>" + "<font color=\"#0D9B6B\">" + " LOGIN" + "</font>")));

    }
}
