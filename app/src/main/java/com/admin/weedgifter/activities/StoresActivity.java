package com.admin.weedgifter.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.StoresListAdapter;
import com.admin.weedgifter.interfaces.StoreItemClickAsynResult;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.ArrayList;

public class StoresActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mGoogleMap;
    private EditText searchBar;
    private ImageView correct, close, filterIcon;
    SupportMapFragment mapFragment;
    private TextView filterLabel, openLabel, curbsideLabel, closedLabel, orderLabel, title, tvCall;
    private FrameLayout mapContainer;
    boolean visi = false, order = false, closed = false, curbside = false, open = false;
    private RecyclerView storesView;
    private StoresListAdapter storesListAdapter;
    private ArrayList<String> list = new ArrayList<>();
    private LinearLayout detailContainer, layoutItemClick, filters;
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
    private Button menuBtn;
    private boolean onBack = false;
    private boolean isFirst = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stores);
        initViews();
        clickListner();
        setStoresView();
    }
    private void setStoresView() {
        storesListAdapter = new StoresListAdapter(this, list, storeItemClickAsynResult);
        storesView.setAdapter(storesListAdapter);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        storesView.setLayoutManager(linearLayoutManager);
        storesListAdapter.notifyDataSetChanged();
    }
    private void clickListner() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        filters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StoresActivity.this, FilterandSortActivity.class);
                startActivity(intent);
            }
        });
        correct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!visi) {
                    mapContainer.setVisibility(View.GONE);
                    visi = true;
                } else {
                    mapContainer.setVisibility(View.VISIBLE);
                    visi = false;
                }
            }
        });
        menuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StoresActivity.this, StoreProductDetailActivity.class);
                startActivity(intent);
            }
        });
        searchBar.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mapContainer.setVisibility(View.GONE);
                    title.setText(getText(R.string.search));
                } else {
                    mapContainer.setVisibility(View.GONE);
                    title.setText(getText(R.string.store));
                }
            }
        });
        orderLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!order) {
                    orderLabel.setBackground(ContextCompat.getDrawable(StoresActivity.this, R.drawable.rounded_bg_selected));
                    filterLabel.setTextColor(Color.parseColor("#0D9B6B"));
                    filterIcon.setColorFilter(ContextCompat.getColor(StoresActivity.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    order = true;
                } else {
                    orderLabel.setBackground(ContextCompat.getDrawable(StoresActivity.this, R.drawable.rounded_bg_unselected));
                    filterLabel.setTextColor(Color.parseColor("#242424"));
                    filterIcon.setColorFilter(ContextCompat.getColor(StoresActivity.this, R.color.colorQuad), PorterDuff.Mode.SRC_IN);
                    order = false;
                }
            }
        });
        closedLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!closed) {
                    closedLabel.setBackground(ContextCompat.getDrawable(StoresActivity.this, R.drawable.rounded_bg_selected));
                    filterLabel.setTextColor(Color.parseColor("#0D9B6B"));
                    filterIcon.setColorFilter(ContextCompat.getColor(StoresActivity.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    closed = true;
                } else {
                    closedLabel.setBackground(ContextCompat.getDrawable(StoresActivity.this, R.drawable.rounded_bg_unselected));
                    filterLabel.setTextColor(Color.parseColor("#242424"));
                    filterIcon.setColorFilter(ContextCompat.getColor(StoresActivity.this, R.color.colorQuad), PorterDuff.Mode.SRC_IN);
                    closed = false;
                }
            }
        });
        curbsideLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!curbside) {
                    curbsideLabel.setBackground(ContextCompat.getDrawable(StoresActivity.this, R.drawable.rounded_bg_selected));
                    filterLabel.setTextColor(Color.parseColor("#0D9B6B"));
                    filterIcon.setColorFilter(ContextCompat.getColor(StoresActivity.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    curbside = true;
                } else {
                    curbsideLabel.setBackground(ContextCompat.getDrawable(StoresActivity.this, R.drawable.rounded_bg_unselected));
                    filterLabel.setTextColor(Color.parseColor("#242424"));
                    filterIcon.setColorFilter(ContextCompat.getColor(StoresActivity.this, R.color.colorQuad), PorterDuff.Mode.SRC_IN);
                    curbside = false;
                }
            }
        });
        openLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!open) {
                    openLabel.setBackground(ContextCompat.getDrawable(StoresActivity.this, R.drawable.rounded_bg_selected));
                    filterLabel.setTextColor(Color.parseColor("#0D9B6B"));
                    filterIcon.setColorFilter(ContextCompat.getColor(StoresActivity.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    open = true;
                } else {
                    openLabel.setBackground(ContextCompat.getDrawable(StoresActivity.this, R.drawable.rounded_bg_unselected));
                    filterLabel.setTextColor(Color.parseColor("#242424"));
                    filterIcon.setColorFilter(ContextCompat.getColor(StoresActivity.this, R.color.colorQuad), PorterDuff.Mode.SRC_IN);
                    open = false;
                }
            }
        });
    }
    private void initViews() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        tvCall = findViewById(R.id.tvCall);
        filters = findViewById(R.id.filters);
        mapContainer = findViewById(R.id.mapContainer);
        correct = findViewById(R.id.correct);
        filterLabel = findViewById(R.id.filterLabel);
        openLabel = findViewById(R.id.openLabel);
        close = findViewById(R.id.close);
        curbsideLabel = findViewById(R.id.curbsideLabel);
        closedLabel = findViewById(R.id.closedLabel);
        orderLabel = findViewById(R.id.orderLabel);
        storesView = findViewById(R.id.storesView);
        searchBar = findViewById(R.id.searchBar);
        title = findViewById(R.id.title);
        title.setText(getText(R.string.store));
        detailContainer = findViewById(R.id.detailContainer);
        layoutItemClick = findViewById(R.id.layoutItemClick);
        menuBtn = findViewById(R.id.menuBtn);
        filterIcon = findViewById(R.id.filterIcon);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
    }
    StoreItemClickAsynResult<String> storeItemClickAsynResult = new StoreItemClickAsynResult<String>() {
        @Override
        public void success(String s) {
            detailContainer.setVisibility(View.GONE);
            layoutItemClick.setVisibility(View.VISIBLE); }
        @Override
        public void error(String error) {}
    };
    @Override
    public void onBackPressed() {
        if (!onBack) {
            onBack = true;
            detailContainer.setVisibility(View.VISIBLE);
            layoutItemClick.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }
}
