package com.admin.weedgifter.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.admin.weedgifter.R;
import com.admin.weedgifter.utils.Common;
import com.admin.weedgifter.utils.Dialog;

public class ChangePasswordActivity extends AppCompatActivity {
    private String currentPassword, newPassword, confirmNewPassword;
    private Button btnSave;
    private EditText etCurrentPassword, etNewPassword, etConfirmNewPassword;
    private TextView currentPasswordError, newPasswordError, confirmNewPasswordError, errorText;
    private ImageView close;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initViews();
        clickListners();
    }
    private void clickListners() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        etCurrentPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etCurrentPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    currentPassword = etCurrentPassword.getText().toString().trim();
                    if (currentPassword.equals("")) {
                        return;
                    } else if (!Common.isPasswordValid(currentPassword)) {
                        errorText.setVisibility(View.GONE);
                        currentPasswordError.setVisibility(View.VISIBLE);
                        newPasswordError.setVisibility(View.GONE);
                        confirmNewPasswordError.setVisibility(View.GONE);
                        etCurrentPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.error_bg));
                        currentPasswordError.setText(getText(R.string.password_valid));
                    } else {
                        etCurrentPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                    }
                }
            }
        });

        etNewPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etNewPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    newPassword = etNewPassword.getText().toString().trim();
                    if (newPassword.equals("")) {
                        return;
                    } else if (!Common.isPasswordValid(newPassword)) {
                        errorText.setVisibility(View.GONE);
                        newPasswordError.setVisibility(View.VISIBLE);
                        confirmNewPasswordError.setVisibility(View.GONE);
                        etNewPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.error_bg));
                        newPasswordError.setText(getText(R.string.password_valid));
                    } else {
                        etNewPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                    }
                }
            }
        });

        etConfirmNewPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etConfirmNewPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    confirmNewPassword = etConfirmNewPassword.getText().toString().trim();
                    if (confirmNewPassword.equals("")) {
                        return;
                    } else if (!newPassword.matches(confirmNewPassword)) {
                        errorText.setVisibility(View.GONE);
                        newPasswordError.setVisibility(View.GONE);
                        confirmNewPasswordError.setVisibility(View.VISIBLE);
                        etConfirmNewPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.error_bg));
                        confirmNewPasswordError.setText(getText(R.string.password_not_match));
                    } else {
                        etConfirmNewPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                        Common.hideKeyboard(ChangePasswordActivity.this,etConfirmNewPassword);
                    }
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentPassword = etCurrentPassword.getText().toString().trim();
                newPassword = etNewPassword.getText().toString().trim();
                confirmNewPassword = etConfirmNewPassword.getText().toString().trim();
                if (currentPassword.equals("") && newPassword.equals("") && confirmNewPassword.equals("")) {
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(getText(R.string.fill_all_fields));
                    etCurrentPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.error_bg));
                    etNewPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.error_bg));
                    etConfirmNewPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.error_bg));
                }else if (!Common.isPasswordValid(currentPassword)) {
                    errorText.setVisibility(View.GONE);
                    currentPasswordError.setVisibility(View.VISIBLE);
                    etCurrentPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.error_bg));
                    currentPasswordError.setText(getText(R.string.password_valid));
                } else if (!Common.isPasswordValid(newPassword)) {
                    errorText.setVisibility(View.GONE);
                    newPasswordError.setVisibility(View.VISIBLE);
                    etNewPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.error_bg));
                    newPasswordError.setText(getText(R.string.password_valid));
                } else if (!confirmNewPassword.matches(newPassword)) {
                    errorText.setVisibility(View.GONE);
                    confirmNewPasswordError.setVisibility(View.VISIBLE);
                    etConfirmNewPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.error_bg));
                    confirmNewPasswordError.setText(getText(R.string.password_valid));
                } else{
                    errorText.setVisibility(View.GONE);
                    currentPasswordError.setVisibility(View.GONE);
                    newPasswordError.setVisibility(View.GONE);
                    confirmNewPasswordError.setVisibility(View.GONE);
                    etCurrentPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.validated_fields));
                    etNewPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.validated_fields));
                    etConfirmNewPassword.setBackground(ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.validated_fields));
                    Dialog.passwordChangedDialog(ChangePasswordActivity.this);

                }
            }
        });
    }

    private void initViews() {
        btnSave = findViewById(R.id.btnSave);
        close = findViewById(R.id.close);
        currentPasswordError = findViewById(R.id.currentPasswordError);
        newPasswordError = findViewById(R.id.newPasswordError);
        confirmNewPasswordError = findViewById(R.id.confirmNewPasswordError);
        etCurrentPassword = findViewById(R.id.etCurrentpassword);
        etNewPassword = findViewById(R.id.etNewPassword);
        etConfirmNewPassword = findViewById(R.id.etConfirmNewPassword);
        errorText = findViewById(R.id.errorText);
    }
}
