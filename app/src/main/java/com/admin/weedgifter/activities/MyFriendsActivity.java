package com.admin.weedgifter.activities;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.FriendsListAdapter;

public class MyFriendsActivity extends AppCompatActivity {
    private RecyclerView myFriendsList;
    private FriendsListAdapter friendsListAdapter;
    private String layout ="myFriends";
    private ImageView close;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_friends);
        initViews();
        clickListner();
        setFriendsList();
    }
    private void clickListner() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void initViews() {
        myFriendsList = findViewById(R.id.myFriendsList);
        close = findViewById(R.id.close);
    }
    private void setFriendsList() {
        friendsListAdapter = new FriendsListAdapter(this,layout);
        myFriendsList.setAdapter(friendsListAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        myFriendsList.setLayoutManager(linearLayoutManager);
        friendsListAdapter.notifyDataSetChanged();
    }
}
