package com.admin.weedgifter.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.admin.weedgifter.R;

public class AmenitiesTypeFilterActivity extends AppCompatActivity {
    private ImageView close;
    private CheckBox allCb, curbCb, labCb, bestCb, atmCb, creditCardCb, bandCb, photosCb;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amenities_type_filter);
        initViews();
        clickListner();
    }
    private void clickListner() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        allCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    allCb.setChecked(true);
                    curbCb.setChecked(true);
                    labCb.setChecked(true);
                    bestCb.setChecked(true);
                    atmCb.setChecked(true);
                    creditCardCb.setChecked(true);
                    photosCb.setChecked(true);
                    bandCb.setChecked(true);
                }
            }
        });
    }
    private void initViews() {
        close = findViewById(R.id.close);
        allCb = findViewById(R.id.allCb);
        curbCb = findViewById(R.id.curbCb);
        labCb = findViewById(R.id.labCb);
        bestCb = findViewById(R.id.bestCb);
        atmCb = findViewById(R.id.atmCb);
        creditCardCb = findViewById(R.id.creditCardCb);
        photosCb = findViewById(R.id.photosCb);
        bandCb = findViewById(R.id.bandCb);
    }
}
