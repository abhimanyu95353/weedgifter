package com.admin.weedgifter.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.NotificationsListAdapter;

public class NotificationsActivity extends AppCompatActivity {
    private ImageView close;
    private RecyclerView notificationsList;
    private NotificationsListAdapter notificationsListAdapter;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        initViews();
        setAccountsList();
    }
    private void setAccountsList() {
        notificationsListAdapter = new NotificationsListAdapter(NotificationsActivity.this);
        notificationsList.setAdapter(notificationsListAdapter);
        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        notificationsList.setLayoutManager(linearLayoutManager);
        notificationsListAdapter.notifyDataSetChanged();
    }
    private void initViews() {
        notificationsList = findViewById(R.id.notificationsList);
        close = findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
