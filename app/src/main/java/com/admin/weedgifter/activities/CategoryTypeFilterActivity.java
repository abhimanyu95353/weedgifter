package com.admin.weedgifter.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.admin.weedgifter.R;

public class CategoryTypeFilterActivity extends AppCompatActivity {
    private ImageView close;
    private CheckBox allCb, salivaCb, hybridCb,indicaCb;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_type_fiter);
        initViews();
        clickListner();
    }
    private void clickListner() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        allCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    allCb.setChecked(true);
                    salivaCb.setChecked(true);
                    indicaCb.setChecked(true);
                    hybridCb.setChecked(true);
                }
            }
        });
    }

    private void initViews() {
        close = findViewById(R.id.close);
        allCb = findViewById(R.id.allCb);
        indicaCb = findViewById(R.id.indicaCb);
        hybridCb = findViewById(R.id.hybridCb);
        salivaCb = findViewById(R.id.salivaCb);

    }
}
