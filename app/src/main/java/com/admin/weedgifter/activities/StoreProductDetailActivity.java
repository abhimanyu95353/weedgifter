package com.admin.weedgifter.activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.admin.weedgifter.R;
import com.admin.weedgifter.fragments.DetailsFragment;
import com.admin.weedgifter.fragments.SeeMenuFragment;
import com.admin.weedgifter.fragments.SingleProductReviewFragment;
import com.admin.weedgifter.utils.Common;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.SharingHelper;
import io.branch.referral.util.ContentMetadata;
import io.branch.referral.util.LinkProperties;
import io.branch.referral.util.ShareSheetStyle;

public class StoreProductDetailActivity extends AppCompatActivity {
    private Button menuBtn;
    private TextView seeMenu, details, reviews;
    FrameLayout seeMenuFrame, detailsFrame, reviewsFrame;
    private ImageView close, shareIcon;
    RelativeLayout parentContainer;
    View menuSelected, menuUnselected, detailsSelected, detailsUnselected, reviewsSelected, reviewsUnselected;
    SeeMenuFragment seeMenuFragment = new SeeMenuFragment();
    DetailsFragment detailsFragment = new DetailsFragment();
    SingleProductReviewFragment reviewsFragment = new SingleProductReviewFragment();
    //    ReviewsFragment reviewsFragment = new ReviewsFragment();
    private LinearLayout seeMenuContainer, detailsContainer, reviewsContainer;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_product_detail);
        initViews();
        clickListner();
        initTabView();
        Uri data = getIntent().getData();
    }
    private void clickListner() {
        shareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Initialising Univeral branch object on share bnt click
                BranchUniversalObject branchUniversalObject = new BranchUniversalObject()
                        .setCanonicalIdentifier("monster/12345")
                        .setTitle("Weed Gift")
                        .setContentDescription("Your friend  has invited you to check this gift, Mr.Frank Castle!")
                        .setContentImageUrl("https://example.com/monster-pic-12345.png")
                        .setContentMetadata(new ContentMetadata().addCustomMetadata("userId", "12345")
                                .addCustomMetadata("userName", "Josh")
                                .addCustomMetadata("monsterName", "Mr. Squiggles"));
                LinkProperties linkProperties = new LinkProperties()
                        .setChannel("facebook")
                        .setFeature("sharing");

                //configuring share sheet style
                ShareSheetStyle ss = new ShareSheetStyle(StoreProductDetailActivity.this, "Check this out!", "This stuff is awesome: ")
                        .setAsFullWidthStyle(true)
                        .setSharingTitle("Share stuff with..");
                branchUniversalObject.showShareSheet(StoreProductDetailActivity.this, linkProperties, ss, new Branch.BranchLinkShareListener() {
                    @Override
                    public void onShareLinkDialogLaunched() {}
                    @Override
                    public void onShareLinkDialogDismissed() {}
                    @Override
                    public void onLinkShareResponse(String sharedLink, String sharedChannel, BranchError error) {}
                    @Override
                    public void onChannelSelected(String channelName) {}
                });
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (Branch.isAutoDeepLinkLaunch(this)) {
            try {
                String autoDeeplinkedValue = Branch.getInstance().getLatestReferringParams().getString("userId");
                //  launch_mode_txt.setText("Launched by Branch on auto deep linking!" + "\n\n" + autoDeeplinkedValue);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            //  launch_mode_txt.setText("Launched by normal application flow");
        }
    }
    private void initTabView() {
        seeMenuContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seeMenuFrame.setVisibility(View.VISIBLE);
                reviewsFrame.setVisibility(View.GONE);
                detailsFrame.setVisibility(View.GONE);
                menuSelected.setVisibility(View.VISIBLE);
                detailsUnselected.setVisibility(View.VISIBLE);
                reviewsUnselected.setVisibility(View.VISIBLE);
                menuUnselected.setVisibility(View.GONE);
                detailsSelected.setVisibility(View.GONE);
                reviewsSelected.setVisibility(View.GONE);
                seeMenu.setTextColor(Color.parseColor("#0D9B6B"));
                details.setTextColor(Color.parseColor("#A7A7A7"));
                reviews.setTextColor(Color.parseColor("#A7A7A7"));
                FragmentTransaction fragementTransaction = getSupportFragmentManager().beginTransaction();
                fragementTransaction.replace(R.id.seeMenuFrame, seeMenuFragment);
                fragementTransaction.commit();
            }
        });
        reviewsContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seeMenuFrame.setVisibility(View.GONE);
                detailsFrame.setVisibility(View.GONE);
                reviewsFrame.setVisibility(View.VISIBLE);
                reviewsSelected.setVisibility(View.VISIBLE);
                detailsUnselected.setVisibility(View.VISIBLE);
                menuUnselected.setVisibility(View.VISIBLE);
                detailsSelected.setVisibility(View.GONE);
                menuSelected.setVisibility(View.GONE);
                reviewsUnselected.setVisibility(View.GONE);
                seeMenu.setTextColor(Color.parseColor("#A7A7A7"));
                details.setTextColor(Color.parseColor("#A7A7A7"));
                reviews.setTextColor(Color.parseColor("#0D9B6B"));
                FragmentTransaction fragementTransaction = getSupportFragmentManager().beginTransaction();
                fragementTransaction.replace(R.id.reviewsFrame, reviewsFragment);
                fragementTransaction.commit();
            }
        });
        detailsContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seeMenuFrame.setVisibility(View.GONE);
                reviewsFrame.setVisibility(View.GONE);
                detailsFrame.setVisibility(View.VISIBLE);
                reviewsUnselected.setVisibility(View.VISIBLE);
                menuUnselected.setVisibility(View.VISIBLE);
                detailsSelected.setVisibility(View.VISIBLE);
                reviewsSelected.setVisibility(View.GONE);
                menuSelected.setVisibility(View.GONE);
                detailsUnselected.setVisibility(View.GONE);
                seeMenu.setTextColor(Color.parseColor("#A7A7A7"));
                details.setTextColor(Color.parseColor("#0D9B6B"));
                reviews.setTextColor(Color.parseColor("#A7A7A7"));
                FragmentTransaction fragementTransaction = getSupportFragmentManager().beginTransaction();
                fragementTransaction.replace(R.id.detailsFrame, detailsFragment);
                fragementTransaction.commit();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void initViews() {
        close = findViewById(R.id.close);
        menuBtn = findViewById(R.id.menuBtn);
        menuBtn.setVisibility(View.GONE);
        seeMenu = findViewById(R.id.seeMenu);
        details = findViewById(R.id.details);
        reviews = findViewById(R.id.reviews);
        shareIcon = findViewById(R.id.shareIcon);
        seeMenuContainer = findViewById(R.id.seeMenuContainer);
        detailsContainer = findViewById(R.id.detailsContainer);
        reviewsContainer = findViewById(R.id.reviewsContainer);
        parentContainer = findViewById(R.id.parentContainer);
        parentContainer.setBackgroundColor(Color.parseColor("#000000"));
        seeMenuFrame = findViewById(R.id.seeMenuFrame);
        detailsFrame = findViewById(R.id.detailsFrame);
        reviewsFrame = findViewById(R.id.reviewsFrame);
        seeMenuFrame.setVisibility(View.VISIBLE);
        reviewsFrame.setVisibility(View.GONE);
        detailsFrame.setVisibility(View.GONE);
        menuSelected = findViewById(R.id.menuSelected);
        menuUnselected = findViewById(R.id.menuUnselected);
        detailsSelected = findViewById(R.id.detailsSelected);
        detailsUnselected = findViewById(R.id.detailsUnselected);
        reviewsSelected = findViewById(R.id.reviewSelected);
        reviewsUnselected = findViewById(R.id.reviewUnselected);
        FragmentTransaction fragementTransaction = getSupportFragmentManager().beginTransaction();
        fragementTransaction.replace(R.id.seeMenuFrame, seeMenuFragment);
        fragementTransaction.commit();
        seeMenu.setTextColor(Color.parseColor("#0D9B6B"));
        details.setTextColor(Color.parseColor("#A7A7A7"));
        reviews.setTextColor(Color.parseColor("#A7A7A7"));
        menuSelected.setVisibility(View.VISIBLE);
        menuUnselected.setVisibility(View.GONE);
        detailsUnselected.setVisibility(View.VISIBLE);
        detailsSelected.setVisibility(View.GONE);
        reviewsSelected.setVisibility(View.GONE);
        reviewsUnselected.setVisibility(View.VISIBLE);
    }
}
