package com.admin.weedgifter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.AccountsListAdapter;

public class SelectBankActivity extends AppCompatActivity {
    private RecyclerView bankList;
    private ImageView close;
    private AccountsListAdapter accountsListAdapter;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_bank);
        initViews();
        setBankList();
        clickListners();
    }
    private void setBankList() {
        accountsListAdapter = new AccountsListAdapter(this,"bankList");
        bankList.setAdapter(accountsListAdapter);
        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        bankList.setLayoutManager(linearLayoutManager);
        accountsListAdapter.notifyDataSetChanged();
    }
    private void initViews() {
        bankList = findViewById(R.id.bankList);
        close = findViewById(R.id.close);
    }
    private void clickListners() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
