package com.admin.weedgifter.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.admin.weedgifter.R;
import com.admin.weedgifter.fragments.MessageFragment;
import com.admin.weedgifter.fragments.SettingsFragment;
import com.admin.weedgifter.fragments.SocialFragment;
import com.admin.weedgifter.fragments.DispensaryFragment;
import com.admin.weedgifter.fragments.HomeFragment;
import com.admin.weedgifter.utils.Common;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

public class DashboardActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, NavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener {
    private GoogleApiClient googleApiClient;
    private GoogleSignInOptions gso;
    private TextView signInLayout, signupLayout;
    private HomeFragment homeFragment = new HomeFragment();
    private DispensaryFragment dispensaryFragment = new DispensaryFragment();
    private MessageFragment messageFragment = new MessageFragment();
    private SocialFragment socialFragment = new SocialFragment();
    private SettingsFragment settingsFragment = new SettingsFragment();
    private Toolbar toolbar;
    private ImageView close, navigation_home;
    private BottomNavigationView bottom_navigation;
    //    int colorInt = getResources().getColor(R.color.errorColor);

    int[][] state = new int[][]{
            new int[]{-android.R.attr.state_selected}, // disabled
            new int[]{android.R.attr.state_selected}, // enabled
            new int[]{-android.R.attr.state_pressed}, // unchecked
            new int[]{android.R.attr.state_pressed}  // pressed
    };
    int[] color = new int[]{
            Color.WHITE,
            Color.parseColor("#0D9B6B"),
            Color.WHITE,
            Color.WHITE
    };
    int[][] states = new int[][]{
            new int[]{-android.R.attr.state_selected}, // disabled
            new int[]{android.R.attr.state_selected}, // enabled
            new int[]{-android.R.attr.state_pressed}, // unchecked
            new int[]{android.R.attr.state_pressed}  // pressed
    };
    int[] colors = new int[]{
            Color.WHITE,
            Color.parseColor("#0D9B6B"),
            Color.WHITE,
            Color.WHITE
    };
    ColorStateList ColorStateList1 = new ColorStateList(state, color);
    ColorStateList ColorStateList2 = new ColorStateList(states, colors);

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        initViews();
        clickListners();
        //   openFragment(dispensaryFragment);
    }

    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void clickListners() {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        signInLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInLayout.setBackground(ContextCompat.getDrawable(DashboardActivity.this, R.drawable.rounded_bg_selected));
                signupLayout.setBackground(ContextCompat.getDrawable(DashboardActivity.this, R.drawable.rounded_bg_unselected));
                signInLayout.setTextColor(Color.WHITE);

                Intent intent = new Intent(DashboardActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        signupLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signupLayout.setBackground(ContextCompat.getDrawable(DashboardActivity.this, R.drawable.rounded_bg_selected));
                signInLayout.setBackground(ContextCompat.getDrawable(DashboardActivity.this, R.drawable.rounded_bg_unselected));
                signupLayout.setTextColor(Color.WHITE);
                Intent intent = new Intent(DashboardActivity.this, SignupActivity.class);
                startActivity(intent);
            }
        });
        bottom_navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        openFragment(homeFragment);
                        //  bottom_navigation.setItemTextColor(ColorStateList1);
                        //  bottom_navigation.setItemIconTintList(ColorStateList2);
                        return true;
                    case R.id.navigation_msg:
                        openFragment(messageFragment);
                        toolbar.setTitle(getText(R.string.message));
                        toolbar.setTitleTextColor(Color.parseColor("#22B573"));
                        //    bottom_navigation.setItemTextColor(ColorStateList1);
                        //  bottom_navigation.setItemIconTintList(ColorStateList2);
                        return true;
                    case R.id.navigation_social:
                        openFragment(socialFragment);
                        toolbar.setTitle(getText(R.string.social));
                        toolbar.setTitleTextColor(Color.parseColor("#22B573"));
                        //     bottom_navigation.setItemTextColor(ColorStateList1);
                        //  bottom_navigation.setItemIconTintList(ColorStateList2);
                        return true;
                    case R.id.navigation_settings:
                        openFragment(settingsFragment);
                        toolbar.setTitle(getText(R.string.settings));
                        toolbar.setTitleTextColor(Color.parseColor("#22B573"));
                        //    bottom_navigation.setItemTextColor(ColorStateList1);
                        //   bottom_navigation.setItemIconTintList(ColorStateList2);
                        return true;
                }
                return false;
            }
        });
    }

    private void initViews() {
        bottom_navigation = findViewById(R.id.bottom_navigation);
        toolbar = findViewById(R.id.toolbar);
        NavigationView navigationView = findViewById(R.id.nav_view);
        Menu menu = navigationView.getMenu();
        MenuItem menuItem = menu.findItem(R.id.nav_logout);
        SpannableString s = new SpannableString(menuItem.getTitle());
        s.setSpan(new TextAppearanceSpan(this, R.style.ItemLogoutDrawerTheme), 0, s.length(), 0);
        menuItem.setTitle(s);
        toolbar.setBackgroundColor(Color.parseColor("#171818"));
        setSupportActionBar(toolbar);
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.drawer);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        close = headerView.findViewById(R.id.closeBtnNavHeader);
        signInLayout = headerView.findViewById(R.id.signInLayout);
        signupLayout = headerView.findViewById(R.id.signupLayout);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        displaySelectedScreen(R.id.nav_home);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        displaySelectedScreen(menuItem.getItemId());
        return true;
    }

    private void displaySelectedScreen(int itemId) {
        //creating fragment object
        Fragment fragment = null;
        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.nav_home:
                fragment = homeFragment;
//                bottom_navigation.setItemTextColor(ColorStateList1);
//                bottom_navigation.setItemIconTintList(ColorStateList2);
                break;
            case R.id.nav_dispensaries:
                toolbar.setTitle(R.string.dispensary);
                fragment = dispensaryFragment;
//                bottom_navigation.setItemTextColor(ColorStateList1);
//                bottom_navigation.setItemIconTintList(ColorStateList2);
                break;
            case R.id.nav_logout:
                logoutAlert();
                break;
        }
        //replacing the fragment
        if (fragment != null) {
            openFragment(fragment);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void logoutAlert() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getText(R.string.logout)).setMessage(getText(R.string.sure))
                .setPositiveButton(getText(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        onLogout();
                        dialogInterface.cancel();
                    }
                }).setNegativeButton(getText(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void onLogout() {
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            SharedPreferences preferences = getSharedPreferences("context", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.clear();
                            editor.apply();
                            Intent intent = new Intent(DashboardActivity.this, MainActivity.class);
                            intent.setAction(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                            Common.displayToast(DashboardActivity.this, getString(R.string.logged_out_success));
                        } else {
                            Common.displayToast(DashboardActivity.this, getString(R.string.server_issue));
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        if (bottom_navigation.getSelectedItemId() == R.id.navigation_home) {
            super.onBackPressed();
            finish();
        } else {
            bottom_navigation.setSelectedItemId(R.id.navigation_home);
        }
    }
}

