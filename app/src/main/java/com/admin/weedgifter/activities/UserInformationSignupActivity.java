package com.admin.weedgifter.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.admin.weedgifter.R;
import com.admin.weedgifter.utils.Common;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class UserInformationSignupActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {
    private TextView signIn, errorText;
    private ImageView close, spinnerArrow;
    private Button btnNext;
    private EditText etAddress, etDob, etName, etCity, etState;
    private String name, gender, dob, address, city, state;
    final Calendar myCalendar = Calendar.getInstance();
    private Spinner etGender;
    private boolean spinnerInit = false;
    DatePickerDialog.OnDateSetListener datePickerDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info_signup);
        initViews();
        clickListners();
    }
    private void clickListners() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserInformationSignupActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNextClick();
            }
        });
        etName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etName.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    name = etName.getText().toString().trim();
                    if (name.equals("")) {
                        return;
                    } else {
                        etName.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                    }
                }
            }
        });
        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add(getString(R.string.male));
        categories.add(getString(R.string.female));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_gender_item, categories);
        dataAdapter.setDropDownViewResource(R.layout.spinner_gender_item);
        etGender.setAdapter(dataAdapter);
        etGender.setOnItemSelectedListener(this);
        etDob.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    Common.hideKeyboard(UserInformationSignupActivity.this, view);
                    etDob.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.edit_text_in_focus_bg));
                    new DatePickerDialog(UserInformationSignupActivity.this, datePickerDialog, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                } else {
                    dob = etDob.getText().toString().trim();
                    if (dob.equals("")) {
                        return;
                    } else {
                        Common.hideKeyboard(UserInformationSignupActivity.this, view);
                        etDob.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                    }
                }
            }
        });
        etAddress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etAddress.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    address = etAddress.getText().toString().trim();
                    if (address.equals("")) {
                        return;
                    } else {
                        etAddress.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                    }
                }
            }
        });
        etCity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etCity.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    city = etCity.getText().toString().trim();
                    if (city.equals("")) {
                        return;
                    } else {
                        etCity.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                    }
                }
            }
        });
        etState.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etState.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    state = etState.getText().toString().trim();
                    if (state.equals("")) {
                        return;
                    } else {
                        etState.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                        Common.hideKeyboard(UserInformationSignupActivity.this, view);
                    }
                }
            }
        });
        etDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(UserInformationSignupActivity.this, datePickerDialog, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }
    private void onNextClick() {
        name = etName.getText().toString();
        dob = etDob.getText().toString();
        address = etAddress.getText().toString();
        city = etCity.getText().toString();
        state = etState.getText().toString();
        if (name.equals("") && address.equals("") && dob.equals("") && address.equals("") && city.equals("") && address.equals("")) {
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
            etName.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.error_bg));
            etAddress.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.error_bg));
            etDob.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.error_bg));
            etCity.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.error_bg));
            etState.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.error_bg));
        } else if (name.equals("")) {
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
            etName.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.error_bg));
        } else if (address.equals("")) {
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
            etAddress.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.error_bg));
        } else if (dob.equals("")) {
            etDob.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.error_bg));
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
        } else if (city.equals("")) {
            etCity.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.error_bg));
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
        } else if (state.equals("")) {
            etState.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.error_bg));
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
        } else {
            Intent intent = new Intent(UserInformationSignupActivity.this, AddPaymentMethodActivity.class);
            startActivity(intent);
        }
    }
    private void initViews() {
        signIn = findViewById(R.id.signIn);
        close = findViewById(R.id.close);
        etAddress = findViewById(R.id.etAddress);
        etDob = findViewById(R.id.etDob);
        etName = findViewById(R.id.etName);
        errorText = findViewById(R.id.errorText);
        etCity = findViewById(R.id.etCity);
        etState = findViewById(R.id.etState);
        etGender = findViewById(R.id.etGender);
        spinnerArrow = findViewById(R.id.spinnerArrow);
        btnNext = findViewById(R.id.btnNext);
        String login = "Login";
        String spanTextLogin = getText(R.string.already_have_an_acc) + login;
        SpannableStringBuilder loginBuilder = new SpannableStringBuilder(spanTextLogin);
        int flg = 0;
        loginBuilder.setSpan(new TextAppearanceSpan(this, R.style.SpanTextAppearance1), 0, spanTextLogin.length(), flg);
        signIn.setText((Html.fromHtml("<font color=\"#ffffff\">" + getText(R.string.already_have_an_acc) + "</font>" + "<font color=\"#0D9B6B\">" + " " + login + "</font>")));
        datePickerDialog = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
    }
    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        etDob.setText(sdf.format(myCalendar.getTime()));
    }
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {}
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        if (spinnerInit) {
            String item = adapterView.getItemAtPosition(position).toString();
            spinnerArrow.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            TextView selectedText = view.findViewById(R.id.selectedItemSpinner);
            selectedText.setBackground(ContextCompat.getDrawable(UserInformationSignupActivity.this, R.drawable.validated_fields));
        }
        spinnerInit = true;
    }
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}
}
