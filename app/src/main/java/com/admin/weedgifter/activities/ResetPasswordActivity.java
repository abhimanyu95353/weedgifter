package com.admin.weedgifter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.admin.weedgifter.R;
import com.admin.weedgifter.utils.Common;

public class ResetPasswordActivity extends AppCompatActivity {
    private String password, confirmPassword;
    private Button btnUpdate;
    private EditText etPassword, etConfirmPassword;
    private TextView passwordError, confirmPasswordError, errorText;
    private ImageView close;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        initViews();
        clickListners();
    }
    private void clickListners() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onUpdateClick();
            }
        });
        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etPassword.setBackground(ContextCompat.getDrawable(ResetPasswordActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    password = etPassword.getText().toString().trim();
                    if (password.equals("")) {
                        return;
                    } else if (!Common.isPasswordValid(password)) {
                        errorText.setVisibility(View.GONE);
                        passwordError.setVisibility(View.VISIBLE);
                        confirmPasswordError.setVisibility(View.GONE);
                        etPassword.setBackground(ContextCompat.getDrawable(ResetPasswordActivity.this, R.drawable.error_bg));
                        passwordError.setText(getText(R.string.password_valid));
                    } else {
                        etPassword.setBackground(ContextCompat.getDrawable(ResetPasswordActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                        passwordError.setVisibility(View.GONE);
                    }
                }
            }
        });
        etConfirmPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressWarnings("UnnecessaryReturnStatement")
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etConfirmPassword.setBackground(ContextCompat.getDrawable(ResetPasswordActivity.this, R.drawable.edit_text_in_focus_bg));
                } else {
                    confirmPassword = etConfirmPassword.getText().toString().trim();
                    if (confirmPassword.equals("")) {
                        return;
                    } else if (!Common.isConfirmPassword(confirmPassword)) {
                        errorText.setVisibility(View.GONE);
                        confirmPasswordError.setVisibility(View.VISIBLE);
                        etConfirmPassword.setBackground(ContextCompat.getDrawable(ResetPasswordActivity.this, R.drawable.error_bg));
                        confirmPasswordError.setText(getText(R.string.password_valid));
                    } else if (!confirmPassword.matches(password)) {
                        confirmPasswordError.setVisibility(View.GONE);
                        errorText.setVisibility(View.VISIBLE);
                        errorText.setText(getText(R.string.password_not_match));
                        Common.hideKeyboard(ResetPasswordActivity.this, etConfirmPassword);
                    } else {
                        etConfirmPassword.setBackground(ContextCompat.getDrawable(ResetPasswordActivity.this, R.drawable.validated_fields));
                        errorText.setVisibility(View.GONE);
                        Common.hideKeyboard(ResetPasswordActivity.this, etConfirmPassword);
                    }
                }
            }
        });
    }
    private void onUpdateClick() {
        password = etPassword.getText().toString().trim();
        confirmPassword = etConfirmPassword.getText().toString().trim();
        if (password.equals("") && confirmPassword.equals("")) {
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(getText(R.string.fill_all_fields));
            etPassword.setBackground(ContextCompat.getDrawable(ResetPasswordActivity.this, R.drawable.error_bg));
            etConfirmPassword.setBackground(ContextCompat.getDrawable(ResetPasswordActivity.this, R.drawable.error_bg));
        } else if (!Common.isPasswordValid(password)) {
            errorText.setVisibility(View.GONE);
            passwordError.setVisibility(View.VISIBLE);
            etPassword.setBackground(ContextCompat.getDrawable(ResetPasswordActivity.this, R.drawable.error_bg));
            passwordError.setText(getText(R.string.password_valid));
        } else if (!Common.isPasswordValid(confirmPassword)) {
            errorText.setVisibility(View.GONE);
            passwordError.setVisibility(View.GONE);
            etConfirmPassword.setBackground(ContextCompat.getDrawable(ResetPasswordActivity.this, R.drawable.error_bg));
            confirmPasswordError.setVisibility(View.VISIBLE);
            confirmPasswordError.setText(getText(R.string.confirm_password_valid));
        } else if (!password.matches(confirmPassword)) {
            Common.hideKeyboard(this, btnUpdate);
            errorText.setVisibility(View.VISIBLE);
            confirmPasswordError.setVisibility(View.GONE);
            passwordError.setVisibility(View.GONE);
            errorText.setText(getText(R.string.password_not_match));
        } else {
            passwordError.setVisibility(View.GONE);
            confirmPasswordError.setVisibility(View.GONE);
            errorText.setVisibility(View.GONE);
            etPassword.setBackground(ContextCompat.getDrawable(ResetPasswordActivity.this, R.drawable.validated_fields));
            etConfirmPassword.setBackground(ContextCompat.getDrawable(ResetPasswordActivity.this, R.drawable.validated_fields));
            btnUpdate.setBackground(ContextCompat.getDrawable(ResetPasswordActivity.this, R.drawable.primary_color_btn_bg_filled));
            Intent intent = new Intent(ResetPasswordActivity.this, PasswordResetSuccessfullyActivity.class);
            startActivity(intent);
        }
    }
    private void initViews() {
        close = findViewById(R.id.close);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        passwordError = findViewById(R.id.passwordError);
        confirmPasswordError = findViewById(R.id.confirmPasswordError);
        errorText = findViewById(R.id.errorText);
        btnUpdate = findViewById(R.id.btnUpdate);
    }
}
