package com.admin.weedgifter.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;

import com.admin.weedgifter.R;
import com.admin.weedgifter.fragments.SingleProductDetailsFragment;
import com.admin.weedgifter.fragments.SingleProductReviewFragment;
import com.admin.weedgifter.utils.Dialog;

public class ProductDetailActivity extends AppCompatActivity {
    private TextView details, reviews, giftMyself, giftFriend;
    private FrameLayout detailsFrame, reviewsFrame;
    private View detailsSelected, detailsUnselected, reviewSelected, reviewUnselected;
    private SingleProductReviewFragment singleProductReviewFragment = new SingleProductReviewFragment();
    private SingleProductDetailsFragment singleProductDetailsFragment = new SingleProductDetailsFragment();
    private LinearLayout detailsContainer, reviewsContainer;
    private ImageView close;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        initViews();
        clickListners();
    }
    private void clickListners() {
        detailsContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detailsFrame.setVisibility(View.VISIBLE);
                reviewsFrame.setVisibility(View.GONE);
                reviewUnselected.setVisibility(View.VISIBLE);
                reviewSelected.setVisibility(View.GONE);
                detailsSelected.setVisibility(View.VISIBLE);
                detailsUnselected.setVisibility(View.GONE);
                details.setTextColor(Color.parseColor("#0D9B6B"));
                reviews.setTextColor(Color.parseColor("#A7A7A7"));
                FragmentTransaction fragementTransaction = getSupportFragmentManager().beginTransaction();
                fragementTransaction.replace(R.id.detailsFrag, singleProductDetailsFragment);
                fragementTransaction.commit();

            }
        });
        reviewsContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detailsFrame.setVisibility(View.GONE);
                reviewsFrame.setVisibility(View.VISIBLE);
                reviewUnselected.setVisibility(View.GONE);
                reviewSelected.setVisibility(View.VISIBLE);
                detailsSelected.setVisibility(View.GONE);
                detailsUnselected.setVisibility(View.VISIBLE);
                details.setTextColor(Color.parseColor("#A7A7A7"));
                reviews.setTextColor(Color.parseColor("#0D9B6B"));
                FragmentTransaction fragementTransaction = getSupportFragmentManager().beginTransaction();
                fragementTransaction.replace(R.id.reviewsFrag, singleProductReviewFragment);
                fragementTransaction.commit();
            }
        });
        giftMyself.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                giftMyself.setBackground(ContextCompat.getDrawable(ProductDetailActivity.this, R.drawable.btn_gift_bg_selected));
                giftFriend.setBackground(ContextCompat.getDrawable(ProductDetailActivity.this, R.drawable.btn_gift_bg_unselected));
                Dialog.giftToMyselfDialog(ProductDetailActivity.this);
            }
        });
        giftFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                giftFriend.setBackground(ContextCompat.getDrawable(ProductDetailActivity.this, R.drawable.btn_gift_bg_selected));
                giftMyself.setBackground(ContextCompat.getDrawable(ProductDetailActivity.this, R.drawable.btn_gift_bg_unselected));
                Intent intent = new Intent(ProductDetailActivity.this, SendGiftToFriendActivity.class);
                startActivity(intent);
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void initViews() {
        details = findViewById(R.id.details);
        reviews = findViewById(R.id.reviews);
        detailsFrame = findViewById(R.id.detailsFrag);
        close = findViewById(R.id.close);
        reviewsFrame = findViewById(R.id.reviewsFrag);
        detailsSelected = findViewById(R.id.detailsSelected);
        detailsUnselected = findViewById(R.id.detailsUnselected);
        reviewSelected = findViewById(R.id.reviewSelected);
        reviewUnselected = findViewById(R.id.reviewUnselected);
        reviewSelected.setVisibility(View.GONE);
        reviewUnselected.setVisibility(View.VISIBLE);
        detailsSelected.setVisibility(View.VISIBLE);
        detailsUnselected.setVisibility(View.GONE);
        giftFriend = findViewById(R.id.giftFriend);
        giftMyself = findViewById(R.id.giftMyself);
        detailsContainer = findViewById(R.id.detailsContainer);
        reviewsContainer = findViewById(R.id.reviewsContainer);
        FragmentTransaction fragementTransaction = getSupportFragmentManager().beginTransaction();
        fragementTransaction.replace(R.id.detailsFrag, singleProductDetailsFragment);
        fragementTransaction.commit();
        details.setTextColor(Color.parseColor("#0D9B6B"));
        reviews.setTextColor(Color.parseColor("#A7A7A7"));
    }
}
