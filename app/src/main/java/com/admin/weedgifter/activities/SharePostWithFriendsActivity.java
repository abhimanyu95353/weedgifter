package com.admin.weedgifter.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.admin.weedgifter.R;
import com.admin.weedgifter.adapters.FriendsListAdapter;
import com.admin.weedgifter.utils.Dialog;

public class SharePostWithFriendsActivity extends AppCompatActivity {
    private RecyclerView friendsList;
    private FriendsListAdapter friendsListAdapter;
    private ImageView close;
    private Button btnSendGift;
    private String layout ="giftToFriend";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_post_with_friends);
        initViews();
        clickListners();
        setFriendsList();
    }
    private void clickListners() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnSendGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void initViews() {
        close = findViewById(R.id.close);
        friendsList = findViewById(R.id.friendsList);
        btnSendGift = findViewById(R.id.btnSendGift);
    }
    private void setFriendsList() {
        friendsListAdapter = new FriendsListAdapter(this,layout);
        friendsList.setAdapter(friendsListAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        friendsList.setLayoutManager(linearLayoutManager);
        friendsListAdapter.notifyDataSetChanged();
    }
}